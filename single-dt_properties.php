<?php get_header();
	$page_layout = get_post_meta ( $post->ID, "_property_layout",true);
	$page_layout = !empty( $page_layout ) ? $page_layout : "content-full-width";
	$show_sidebar = $show_left_sidebar = $show_right_sidebar =  false;
	$sidebar_class = "";

	switch ( $page_layout ) {
		case 'with-left-sidebar':
			$page_layout = "page-with-sidebar with-left-sidebar";
			$show_sidebar = $show_left_sidebar = true;
			$sidebar_class = "secondary-has-left-sidebar";
		break;

		case 'with-right-sidebar':
			$page_layout = "page-with-sidebar with-right-sidebar";
			$show_sidebar = $show_right_sidebar = true;
			$sidebar_class = "secondary-has-right-sidebar";
		break;

		case 'both-sidebar':
			$page_layout = "page-with-sidebar page-with-both-sidebar";
			$show_sidebar = $show_right_sidebar = $show_left_sidebar = true;
			$sidebar_class = "secondary-has-both-sidebar";
		break;

		case 'content-full-width':
		default:
			$page_layout = "content-full-width";
		break;
	}

	if ( $show_sidebar ):
		if ( $show_left_sidebar ):?>
			<!-- Secondary Left -->
			<section id="secondary-left" class="secondary-sidebar <?php echo esc_attr($sidebar_class);?>">
				<?php get_sidebar( 'left' );?>
			</section><?php
		endif;
	endif;?>

<!-- ** Primary Section ** -->
	<section id="primary" class="<?php echo esc_attr($page_layout);?>"><?php
		if( have_posts() ) :
			while( have_posts() ):
				the_post();?>

					<div class="property-single-detail">
						<ul class="single-property-info">
							<?php $price = get_post_meta ( $post->ID, "_property_price",true);
								$price = dt_wp_kses( $price ); 

								if( !empty($price) ):
									$currency = dttheme_option("property","currency");
									$currency = dt_wp_kses( $currency ); 

									echo "<li class='price'>{$currency}{$price}</li>";
								endif;

								the_terms($post->ID,'property_type','<li>'.__('Type','dt_themes').': ',', ','</li>');

								the_terms($post->ID,'property_location','<li>'.__('Location','dt_themes').': ',', ','</li>'); 

								$hide_area = get_post_meta ( $post->ID, "_hide_area",true);
								$area = get_post_meta ( $post->ID, "_area",true);
								$area = dt_wp_kses( $area ); 

								if( empty($hide_area) && !empty($area) ):
									$areaunit = dttheme_option("property","area_unit");
									$areaunit = dt_wp_kses( $areaunit ); 

									echo '<li>'.__('Area',"dt_themes")." : <span>{$area} {$areaunit}</span></li>"; 
								endif;

								$hide_beds = get_post_meta ( $post->ID, "_hide_bedrooms",true);
								$beds = get_post_meta ( $post->ID, "_bedrooms",true);
								if( empty($hide_beds) && !empty($beds) ):
									$beds = dt_wp_kses( $beds ); 
									echo '<li>'.__('Beds',"dt_themes")." : <span>{$beds}</span></li>"; 
								endif;

								$hide_baths = get_post_meta ( $post->ID, "_hide_bathrooms",true);
								$baths = get_post_meta ( $post->ID, "_bathrooms",true);
								if( empty($hide_baths) && !empty($baths) ):
									$baths = dt_wp_kses( $baths ); 
									echo '<li>'.__('Baths',"dt_themes")." : <span>{$baths}</span></li>"; 
								endif;

								$hide_parking = get_post_meta ( $post->ID, "_hide_parking",true);
								$parking = get_post_meta ( $post->ID, "_parking",true);
								if( empty($hide_parking) && !empty($parking) ):
									$parking = dt_wp_kses( $parking ); 
									echo '<li>'.__('Garage',"dt_themes")." : <span>{$parking}</span></li>"; 
								endif;

								$hide_floors = get_post_meta ( $post->ID, "_hide_floors",true);
								$floors = get_post_meta ( $post->ID, "_floors",true);
								if( empty($hide_floors) && !empty($floors) ):
									$floors = dt_wp_kses( $floors ); 
								echo '<li>'.__('Floors',"dt_themes")." : <span>{$floors}</span></li>"; 
								endif;

									echo '<li>'.__('Posted On','dt_themes').': <span>'. get_the_date('M').get_the_date(' d, ').get_the_date('Y').'</span> </li>';
									echo "<li class='print-icon'><a href='javascript:window.print()'><i class='fa fa-print'></i></a></li>";?>
						</ul>

						<div class="property-gallery-container">
							<?php 	$contract_type = "";
							$contract_type_slug = "";
							$contract_type_link = "";

							$contract = get_the_terms( $post->ID, 'contract_type' );
							if( is_object( $contract) || is_array($contract) ){
								foreach ( $contract as $c ) :
									$contract_type = $c->name;
									$contract_type_slug = $c->slug;
									$contract_type_link = get_term_link( $contract_type_slug, 'contract_type' );
								endforeach;
							}

							if( !empty( $contract_type ) ):?>
								<span class="property-contract-type <?php echo esc_attr($contract_type_slug);?>"><?php
									echo "<a href='".esc_url($contract_type_link)."'>".esc_html($contract_type)."</a>";?></span>
							<?php endif;?>


							<ul class="property-gallery"><?php
								$media = get_post_meta ( $post->ID, "_property_media",true);
								$media = is_array($media) ? $media : array();
								if (array_key_exists ( "items", $media )) {
									foreach( $media["items_name"] as $k => $v ){
										$current_item = $media["items"][$k];
										if( $v === "video" ){
											echo "<li>".wp_oembed_get( $current_item )."</li>";
										} else {
											echo "<li> <img src='".esc_url($current_item)."' alt='' title='' /></li>";
										}
									}
								}else{
									echo "<li><img src='http://placehold.it/1060x718&text=Property' alt='' title=''/></li>";
								}?>
							</ul>
							<?php if (array_key_exists ( "items", $media ) && ( count($media["items"]) > 1) ) {

									echo '<div id="bx-pager">';
									foreach( $media["items_name"] as $k => $v ){
										$current_item = $media["items_thumbnail"][$k];
										if( $v === "video" ) {
											$url = IAMD_BASE_URL."images/property-video-placeholder.png";
											echo "<a data-slide-index='{$k}'><img src='".esc_url($url)."' alt='' title=''/></a>";
										} else {
											echo "<a data-slide-index='{$k}'> <img src='".esc_url($current_item)."' alt='' title='' /></a>";
										}	
									}
									echo '</div>';
								}?>
						</div>

						<div class="clear"> </div> 
						<div class="dt-sc-hr-invisible"> </div>

						<?php the_content(); ?>

						<div class="clear"> </div>
						<div class="dt-sc-hr-invisible-small"> </div>

						<?php $amenities_objs = get_the_terms( $post->ID,'property_amenities');
							if( is_array( $amenities_objs ) ) {
								echo '<h4>'.__('Amenities','dt_themes').'</h4>';
								echo '<ul class="amenities-list dt-sc-fancy-list rounded-tick">';
								echo get_the_term_list( $post->ID, 'property_amenities', '<li>', '</li><li>', '</li>' );
								echo "</ul>";
							}

						echo '<div class="social-share">';
						echo '<h4>'.__('Social Share','dt_themes').'</h4>';
							echo '<div class="sb-container">';
							show_fblike('property');
							echo '</div>';
							echo '<div class="sb-container">';
							show_googleplus('property');
							echo '</div>';
							echo '<div class="sb-container">';
							show_twitter('property');
							echo '</div>';
							echo '<div class="sb-container">';
							show_stumbleupon('property');
							echo '</div>';
							echo '<div class="sb-container">';
							show_linkedin('property');
							echo '</div>';
							echo '<div class="sb-container">';
							show_delicious('property');
							echo '</div>';
							echo '<div class="sb-container">';
							show_pintrest('property');
							echo '</div>';
							echo '<div class="sb-container">';
							show_digg('property');
							echo '</div>';
						echo '</div>';

						echo '<div class="social-bookmark">';
						echo '<h4>'.__('Social Bookmarks','dt_themes').'</h4>';
						dttheme_social_bookmarks('sb-property');
						echo '</div>'; ?>

						<div class="clear"> </div>
						<div class="dt-sc-hr-invisible-small"> </div>

						<?php $agents = get_post_meta ( $post->ID, "_property_agents",true);
							$agents = is_array($agents) ? array_filter($agents) : array();
							$agents_email_ids = array();

							if( count($agents) > 0 ):
								echo '<div id="single-property-agent-info" class="column dt-sc-one-half first">';
									$prefix = $sufix = "";
									if( count($agents) > 1 ):
										echo '<h4>'.__('Agents Information','dt_themes').'</h4>';
										echo '<div class="dt-sc-agent-carousel-wrapper">';
										echo '	<ul class="dt-sc-agent-carousel">';
										$prefix = "<li>";
										$sufix = "</li>";
									else:
										echo '<h4>'.__('Agent Information','dt_themes').'</h4>';
									endif;

									foreach( $agents as $agent_id ):
										$agent = get_post($agent_id,'ARRAY_A');
										$link = get_permalink($agent_id);
										echo $prefix;
										echo '<div class="dt-sc-agents-list">';

											echo '	<div class="dt-sc-agent-thumb">';
											 		if( has_post_thumbnail($agent_id) ) :
											 			echo get_the_post_thumbnail($agent_id,"full");
											 		else:
											 			echo '<img src="http://placehold.it/400x420&text=Image" />';
											 		endif;	
											echo '	</div>';

											echo '	<div class="dt-sc-agent-details">';
												echo "	 <h4><a href='{$link}'>{$agent['post_title']}</a></h4>";

												echo '	 <div class="dt-sc-agent-contact">';
												 			$agent_mobile = get_post_meta ( $agent_id,"_agent_mobile",true);
												 			$agent_mobile = dt_wp_kses( $agent_mobile ); 

												 			$agent_phone = get_post_meta ( $agent_id,"_agent_phone",true);
												 			$agent_phone = dt_wp_kses( $agent_phone ); 

												 			$agent_email_id = get_post_meta ( $agent_id,"_agent_email_id",true);
												 			array_push($agents_email_ids, $agent_email_id);

												 			if( !empty( $agent_mobile ) )
												 				echo "<p> <span class='fa fa-mobile'> </span> {$agent_mobile} </p>";

												 			if( !empty( $agent_phone ) )
												 				echo "<p> <span class='fa fa-phone'> </span> {$agent_phone} </p>";

												 			if( !empty( $agent_email_id ) ):
												 				$agent_email_id = dt_wp_kses( $agent_email_id ); 
												 				echo "<p> <span class='fa fa-envelope'> </span> <a href='mailto:{$agent_email_id}'>{$agent_email_id}</a></p>";
												 			endif;
												echo '	 </div>';
											echo '	</div>';

											echo '	<div class="dt-sc-agent-content">'.do_shortcode($agent['post_content']).'</div>';

											$socials =  get_post_meta( $agent_id, "_agent_social",true);
											if( !empty($socials) ) :
												echo '<ul class="dt-sc-social-icons">';
												foreach( $socials as $k => $v ):
													$i1 = IAMD_BASE_URL."images/sociable/hover/{$k}";
													$i2 = IAMD_BASE_URL."images/sociable/{$k}";
													$class = explode(".",$k);
													$class = $class[0];
													echo "<li class='".esc_attr($class)."'><a href='".esc_url($v)."'><img src='".esc_url($i1)."'/><img src='".esc_url($i2)."'/></a></li>";
												endforeach;
												echo '</ul>';
											endif;	
										echo '</div>';
										echo $sufix;
									endforeach;

									if( count($agents) > 1 ):
										echo '	</ul>';
										echo '<div class="carousel-arrows">';
										echo '	<a href="" class="agents-prev"> </a>';
										echo '	<a href="" class="agents-next"> </a>';
										echo '</div>';
										echo '</div>';
									endif;
								echo '</div>';

								echo '<div id="single-property-enquiry-form" class="column dt-sc-one-half">';
									echo '<h4>'.__(' Enquiry Form','dt_themes').'</h4>';?>

										<form id="property-enquiry" method="post" action="<?php echo get_template_directory_uri()."/framework/property-enquiry.php";?>">

											<div id="message"></div>

											<div class="column dt-sc-one-half first">
												<p>
													<label><?php _e('First Name','dt_themes');?></label>
													<input type="text" name="txtfname" required="required">
												</p>
											</div>

											<div class="column dt-sc-one-half">
												<p>
													<label><?php _e('Last Name','dt_themes');?></label>
													<input type="text" name="txtlname">
												</p>
											</div>

											<div class="column dt-sc-one-half first">
												<p>
													<label><?php _e('Email','dt_themes');?></label>
													<input type="email" name="txtemail" required="required">
												</p>
											</div>

											<div class="column dt-sc-one-half">
												<p>
													<label><?php _e('Phone','dt_themes');?></label>
													<input type="tel" name="phone">
												</p>
											</div>

					                        <p>
					                            <label><?php _e('Message','dt_themes');?></label>
					                            <textarea cols="" rows="" name="message" required="required"></textarea>
					                        </p>

					                        <input type="hidden" name="admin_emailid" value="<?php echo  esc_attr( get_option('admin_email') );?>">
					                        <input type="hidden" name="txtproperty" value="<?php the_title();?>">
					                        <input type="hidden" name="property_link" value="<?php the_permalink();?>">
					                        <?php foreach( $agents_email_ids as $agent_email_id): ?>
					                        	<input type="hidden" name="agents[]" value="<?php echo $agent_email_id;?>"> 
						                    <?php endforeach;?>
					                        <input type="submit" value="<?php _e('Send','dt_themes');?>">
										</form><?php
								echo '</div>';
							endif;?>

						<?php $featured_video = get_post_meta ( $post->ID, "_property_featured_video",true);
							if( !empty($featured_video) ): ?>

								<div class="clear"> </div>
								<div class="dt-sc-hr-invisible"> </div>
								<div id="property-featured-video-container">
									<h4><?php _e('Property Video Section','dt_themes');?></h4>
									<div class="property-featured-video"><?php echo wp_oembed_get( $featured_video ); ?></div>
								</div><?php
							endif;

							$gps = get_post_meta ( $post->ID, "_property_gps",true);
							$gps = is_array($gps) ? $gps : array();

							if( array_key_exists("latitude", $gps) && array_key_exists("longitude", $gps) ):
								$address = get_post_meta ( $post->ID, "_property_address",true);
								$address = dt_wp_kses( $address );

								$property_type = get_the_terms( $post->ID, 'property_type' );
								if( is_object( $property_type) || is_array($property_type) ){
									foreach ( $property_type as $c ) :
					   					$icon = get_option( "taxonomy_term_$c->term_id" );
					   					$icon = $icon['icon'];
					   					$icon = !empty( $icon ) ? $icon : get_template_directory_uri().'/images/default-marker.png';
									endforeach;
								}else {
									$icon = get_template_directory_uri().'/images/default-marker.png';
								}?>
								<div class="clear"> </div>
								<div class="dt-sc-hr-invisible"> </div>
								<h5><?php _e('Location Info','dt_themes');?></h5>
									<div class="dt-sc-map-container">
										<div id="single-gmap" style="margin: 0;padding: 0;height: 100%;"
											data-title = "<?php the_title();?>" 
											data-lat="<?php echo $gps['latitude'];?>" 
											data-lng="<?php echo $gps['longitude'];?>"
											data-address="<?php echo $address;?>"
											data-icon = "<?php echo $icon;?>"></div>
									</div><?php
							endif;?>
 					</div>
				<?php edit_post_link( __( ' Edit ','dt_themes' ) );
			endwhile;
		endif;?>

		<?php $related = get_post_meta ( $post->ID, "_related_properties",true);
			  $ptype  = wp_get_object_terms( $post->ID, 'property_type');
			  $ptypeid = is_array( $ptype ) && sizeof($ptype) > 0 ? $ptype[0]->term_id : $ptype;
		if( !empty($related)  && !empty($ptypeid) ):
			$args = array( 'orderby' => 'rand',
				'showposts' => '3' ,
				'post__not_in' => array($post->ID),
				'tax_query' => array( array( 'taxonomy'=>'property_type', 'field'=>'id', 'operator'=>'IN', 'terms'=>array($ptypeid) )));

			query_posts($args);
			if( have_posts() ): ?>
			<!-- Related Properties -->
            <div id="dt-related-properties">
				<div class="clear"> </div>
				<div class="dt-sc-hr-invisible-medium"> </div>            
				<h2 class="border-title"> <span><?php _e('Related Properties','dt_themes');?></span> </h2>
			<?php
				$count = 1;
				while( have_posts() ):
					the_post();
					$the_id = get_the_ID();
					$permalink = get_permalink($the_id);
					$title = get_the_title($the_id);

					$contract_type = "";
					$contract_type_slug = "";
					$contract = get_the_terms( $the_id, 'contract_type' );
					if( is_object( $contract) || is_array($contract) ){
						foreach ( $contract as $c ) :
							$contract_type = $c->name;
							$contract_type_slug = $c->slug;
							$contract_type_link = get_term_link( $contract_type_slug, 'contract_type' );
						endforeach;
					}

					$property_type = "";
					$property_type_link = "";
					$property_type = get_the_terms( $the_id, 'property_type' );
					if( is_object( $property_type) || is_array($property_type) ){
						foreach ( $property_type as $c ) :
							$property_type = $c->name;
							$property_type_link = get_term_link( $c->slug, 'property_type' );
						endforeach;
					}

					$media = get_post_meta ( $the_id, "_property_media",true);
					$media = is_array($media) ? $media : array();

					$price = get_post_meta ( $the_id, "_property_price",true);
					$price = dt_wp_kses( $price ); 
					
					$first = ( $count === 1 ) ? " first" : "";?>
					<!-- Property Item -->
					<div class="column dt-sc-one-third <?php echo $first;?>">
						<div class="property-item">

							<div class="property-thumb"><?php 
								if( !empty( $contract_type ) ):?>
									<span class="property-contract-type <?php echo esc_html($contract_type_slug);?>"><?php 
										echo "<a href='".esc_url($contract_type_link)."'>".esc_html($contract_type)."</a>";?></span><?php 
								endif;?>
								<ul class="porperty-slider"><?php
									if( array_key_exists("items_name",$media) ) {
										foreach( $media["items_name"] as $key => $item ) {
											$current_item = $media["items"][$key];
											if( "video" === $item ) {
												echo "<li>".wp_oembed_get( $current_item )."</li>";
											} else {
												echo "<li> <img src='".esc_url($current_item)."' alt='' title='' /></li>";
											}
										}
									} else {
										echo "<li> <img src='http://placehold.it/1060x718&text=Real Home' alt='' title=''/></li>";
									}?></ul>

								<div class="property-thumb-meta"><?php
									if( !empty($property_type) ) 
										echo "<span class='property-type'><a href='".esc_url($property_type_link)."'>".esc_html($property_type)."</a></span>";

									if( !empty($price) )
										echo "<span class='property-price'>{$currency} {$price} </span>";?>
								</div>	
							</div>

							<div class="property-details">
								<div class="property-details-inner">
									<h2><a href='<?php echo esc_url($permalink);?>'><?php echo esc_html($title);?></a></h2>
									<h3><?php $address = get_post_meta ( $the_id, "_property_address",true);
										$address = dt_wp_kses( $address );
										echo $address; ?></h3>
									<ul class="property-meta"><?php
										$hide_area = get_post_meta ( $the_id, "_hide_area",true);
										$area = get_post_meta ( $the_id, "_area",true);

										if( empty($hide_area) && !empty($area) ):
											$area = dt_wp_kses( $area );
											$areaunit = dttheme_option("property","area_unit");
											$areaunit = dt_wp_kses( $areaunit );

											echo '<li>'.__('Area',"dt_themes")." : <span>{$area} {$areaunit}</span></li>"; 
										endif;

										$hide_beds = get_post_meta ( $the_id, "_hide_bedrooms",true);
										$beds = get_post_meta ( $the_id, "_bedrooms",true);
										if( empty($hide_beds) && !empty($beds) ):
											$bedrooms = dt_wp_kses( $bedrooms );
											echo '<li>'.__('Beds',"dt_themes")." : <span>{$beds}</span></li>"; 
										endif;

										$hide_baths = get_post_meta ( $the_id, "_hide_bathrooms",true);
										$baths = get_post_meta ( $the_id, "_bathrooms",true);
										if( empty($hide_baths) && !empty($baths) ):
											$baths = dt_wp_kses( $baths );
											echo '<li>'.__('Baths',"dt_themes")." : <span>{$baths}</span></li>"; 
										endif;

										$hide_parking = get_post_meta ( $the_id, "_hide_parking",true);
										$parking = get_post_meta ( $the_id, "_parking",true);
										if( empty($hide_parking) && !empty($parking) ):
											$parking = dt_wp_kses( $parking );
											echo '<li>'.__('Garage',"dt_themes")." : <span>{$parking}</span></li>"; 
										endif;

										$hide_floors = get_post_meta ( $the_id, "_hide_floors",true);
										$floors = get_post_meta ( $post->ID, "_floors",true);
										if( empty($hide_floors) && !empty($floors) ):
											$floors = dt_wp_kses( $floors );
											echo '<li>'.__('Floors',"dt_themes")." : <span>{$floors}</span></li>"; 
										endif;?>
									</ul>
								</div>
							</div>
						</div>
					</div><?php
					$count++;
				endwhile; ?>
			</div><?php	
			endif; ?>
			<!-- Related Properties End -->	
		<?php endif;?>
	</section><!-- ** Primary Section End ** --><?php
	if ( $show_sidebar ):
		if ( $show_right_sidebar ): ?>
			<!-- Secondary Right -->
			<section id="secondary-right" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>">
				<?php get_sidebar( 'right' );?>
			</section><?php
		endif;
	endif;?>
<?php get_footer(); ?>