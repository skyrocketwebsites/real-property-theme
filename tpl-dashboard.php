<?php /*Template Name: Dashbaord Template */
get_header();

	$tpl_default_settings = get_post_meta( $post->ID, '_tpl_default_settings', TRUE );
	$tpl_default_settings = is_array( $tpl_default_settings ) ? $tpl_default_settings  : array();

	$page_layout  = array_key_exists( "layout", $tpl_default_settings ) ? $tpl_default_settings['layout'] : "content-full-width";
	$show_sidebar = $show_left_sidebar = $show_right_sidebar =  false;
	$sidebar_class = "";

	switch ( $page_layout ) {
		case 'with-left-sidebar':
			$page_layout = "page-with-sidebar with-left-sidebar";
			$show_sidebar = $show_left_sidebar = true;
			$sidebar_class = "secondary-has-left-sidebar";
		break;

		case 'with-right-sidebar':
			$page_layout = "page-with-sidebar with-right-sidebar";
			$show_sidebar = $show_right_sidebar	= true;
			$sidebar_class = "secondary-has-right-sidebar";
		break;

		case 'both-sidebar':
			$page_layout = "page-with-sidebar page-with-both-sidebar";
			$show_sidebar = $show_right_sidebar	= $show_left_sidebar = true;
			$sidebar_class = "secondary-has-both-sidebar";
		break;

		case 'content-full-width':
		default:
			$page_layout = "content-full-width";
		break;
	}

	if ( $show_sidebar ):
		if ( $show_left_sidebar ): ?>
			<!-- Secondary Left -->
			<section id="secondary-left" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>">
				<?php get_sidebar( 'left' );?>
			</section><?php
		endif;
	endif;?>

	<!-- ** Primary Section ** -->
	<section id="primary" class="<?php echo esc_attr( $page_layout );?>">
    	<?php if( isset($_REQUEST) && $_REQUEST['action'] == 'added'  ) {
				dt_property_form_result( __('Property added successfully','dt_themes') );
		}elseif( isset($_REQUEST) && $_REQUEST['action'] == 'edited' ){
			dt_property_form_result( __('Property edited successfully','dt_themes') );
		}elseif( isset($_REQUEST) && $_REQUEST['action'] == 'deleted' ){
			dt_property_form_result( __('Property deleted successfully','dt_themes') );
		}?>
	<?php
	if( is_user_logged_in() ) {

		global $userdata;

		if( isset($_REQUEST['action']) && $_REQUEST['action'] === "add"  ):

			if( (key($userdata->caps)) != 'administrator' ) {
				
				if( dttheme_is_plugin_active('s2member/s2member.php') ) {

					$current_user_level = S2MEMBER_CURRENT_USER_ACCESS_LEVEL;
					$today = getdate();
	
					switch( $current_user_level ):
				
						#Members are allowed to post per year ( you can set this in admin panel )
						case 0:	
							$allowed_post = dttheme_option("property","s2-0-allowed-post");
							$period =  dttheme_option("property","s2-0-allowed-period");
						break;
						
						case 1:
							$allowed_post = dttheme_option("property","s2-1-allowed-post");
							$period =  dttheme_option("property","s2-1-allowed-period");
						break;
						
						case 2:	
							$allowed_post = dttheme_option("property","s2-2-allowed-post");	
							$period =  dttheme_option("property","s2-2-allowed-period");
						break;
						
						case 3:	
							$allowed_post = dttheme_option("property","s2-3-allowed-post");
							$period =  dttheme_option("property","s2-3-allowed-period");
						break;
						
						case 4:
							$allowed_post = dttheme_option("property","s2-4-allowed-post"); 
							$period =  dttheme_option("property","s2-4-allowed-period");
						break;
					endswitch;

					switch($period):
						case 'Day':
							$day = $today['mday'];
							$query = "author={$userdata->ID}&day={$day}&post_type=dt_properties&posts_per_page=-1";
						break;

						case 'Week':
							$week = date('W');
							$year = date('Y');
							$query = "author={$userdata->ID}&year={$year}&w={$week}&post_type=dt_properties&posts_per_page=-1";
						break;

						case 'Month':
							$month = $today['mon'];
							$query = "author={$userdata->ID}&month={$month}&post_type=dt_properties&posts_per_page=-1";
						break;

						default:
						case 'Year':
							$year = $today['year'];
							$query = "author={$userdata->ID}&year={$year}&post_type=dt_properties&posts_per_page=-1";
						break;
					endswitch;

					$query = new WP_Query($query);
					$count = $query->post_count;


					if( $allowed_post > $count  ) {
						dt_property_form( $userdata->ID , $_REQUEST['action'] , $property_id = 0);
					} else{
						$msg = stripslashes(dttheme_option("property","s2-member-msg"));
						echo do_shortcode($msg);
					}
				} else {
					dt_property_form( $userdata->ID , $_REQUEST['action'] , $property_id = 0);	
				}

			} else {
				dt_property_form( $userdata->ID , $_REQUEST['action'] , $property_id = 0);
			}	

		elseif( isset($_REQUEST['action']) && $_REQUEST['action'] === "delete" ):
			dt_delete_property( $_REQUEST['pid']);
		elseif( isset($_REQUEST['action']) && $_REQUEST['action'] === "edit"  ):
			dt_property_form( $userdata->ID , $_REQUEST['action'] , $_REQUEST['pid'] );
		else:
			dt_list_properties_by_user( $userdata->ID );
		endif;
	} else {

		if( have_posts() ):
			while( have_posts() ):
				the_post();
				get_template_part( 'framework/loops/content', 'page' );
			endwhile;
		endif;

		$link = dt_get_page_permalink_by_its_template('tpl-login.php');
		$link = !empty( $link ) ? esc_url($link) : home_url();

		echo "<a href='{$link}' class='dt-sc-button small filled with-icon'><i class='fa fa-sign-in'></i>".__("Login","dt_themes")."</a>";


	}?>
	</section><!-- ** Primary Section End ** --><?php

	if ( $show_sidebar ):
		if ( $show_right_sidebar ): ?>
			<!-- Secondary Right -->
			<section id="secondary-right" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>">
				<?php get_sidebar( 'right' );?>
			</section><?php
		endif;
	endif;?>
<?php get_footer(); ?>