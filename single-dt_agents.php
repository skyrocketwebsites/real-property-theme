<?php get_header();
	$page_layout = get_post_meta ( $post->ID , "_agent_layout",true);
	$page_layout = !empty( $page_layout ) ? $page_layout : "content-full-width";
	$show_sidebar = $show_left_sidebar = $show_right_sidebar =  false;
	$sidebar_class = "";

	switch ( $page_layout ) {
		case 'with-left-sidebar':
			$page_layout = "page-with-sidebar with-left-sidebar";
			$show_sidebar = $show_left_sidebar = true;
			$sidebar_class = "secondary-has-left-sidebar";
		break;

		case 'with-right-sidebar':
			$page_layout = "page-with-sidebar with-right-sidebar";
			$show_sidebar = $show_right_sidebar = true;
			$sidebar_class = "secondary-has-right-sidebar";
		break;

		case 'both-sidebar':
			$page_layout = "page-with-sidebar page-with-both-sidebar";
			$show_sidebar = $show_right_sidebar = $show_left_sidebar = true;
			$sidebar_class = "secondary-has-both-sidebar";
		break;

		case 'content-full-width':
		default:
			$page_layout = "content-full-width";
		break;
	}

	if ( $show_sidebar ):
		if ( $show_left_sidebar ):?>
			<!-- Secondary Left -->
			<section id="secondary-left" class="secondary-sidebar <?php echo esc_attr($sidebar_class);?>">
				<?php get_sidebar( 'left' );?>
			</section><?php
		endif;
	endif;?>

<!-- ** Primary Section ** -->
	<section id="primary" class="<?php echo esc_attr($page_layout);?>"><?php
		if( have_posts() ) :
			while( have_posts() ):
				the_post();?>
				<div class="dt-sc-agent-single">
			<div class="dt-sc-agents-list">

				<div class="dt-sc-agent-thumb"><?php
					if( has_post_thumbnail() ):
						the_post_thumbnail("full");
					else: ?>
						<img src="http://placehold.it/400x420&text=Image" alt="<?php printf(esc_attr__('%s'),the_title_attribute('echo=0'));?>" title="<?php printf(esc_attr__('%s'),the_title_attribute('echo=0'));?>" /><?php
					endif;?>	
				</div>

				<div class="dt-sc-agent-details">
					<h4><?php the_title();?></h4>
					<div class="dt-sc-agent-contact"><?php
						$agent_mobile = get_post_meta ( $post->ID, "_agent_mobile",true);
						$agent_mobile = dt_wp_kses( $agent_mobile ); 

						$agent_phone = get_post_meta ( $post->ID, "_agent_phone",true);
						$agent_phone = dt_wp_kses( $agent_phone ); 

						$agent_email_id = get_post_meta ( $post->ID, "_agent_email_id",true);
						$agent_email_id = dt_wp_kses( $agent_email_id ); 

						if( !empty( $agent_mobile ) )
							echo "<p> <span class='fa fa-mobile'> </span> {$agent_mobile} </p>";

						if( !empty( $agent_phone ) )
							echo "<p> <span class='fa fa-phone'> </span> {$agent_phone} </p>";

						if( !empty( $agent_email_id ) )
							echo "<p> <span class='fa fa-envelope'> </span> <a href='mailto:{$agent_email_id}'>{$agent_email_id}</a></p>";?>
					</div>

					<div class="dt-sc-agent-content"> <?php the_content();?></div>
					<?php $socials =  get_post_meta( $post->ID, "_agent_social",true);
						if( !empty($socials) ):?>
							<ul class="dt-sc-social-icons"><?php
								foreach( $socials as $k => $v ):
									$i1 = IAMD_BASE_URL."images/sociable/hover/{$k}";
									$i2 = IAMD_BASE_URL."images/sociable/{$k}";
									$class = explode(".",$k);
									$class = $class[0];
									echo "<li class='".esc_attr($class)."'><a href='".esc_url($v)."'><img src='".esc_url($i1)."'/><img src='".esc_url($i2)."'/></a></li>";
								endforeach;?>
							</ul><?php
						endif;?>	
				</div>
			</div>
				</div><?php

				edit_post_link( __( ' Edit ','dt_themes' ) );
			endwhile;
		endif;

		$show_assigned_property = get_post_meta ( $post->ID, "_show_assigned_property",true);

		if( !empty($show_assigned_property) ):?>

			<h2 class="border-title"> <span><?php _e('Assigned Properties','dt_themes');?></span> </h2>

			<div class="dt-sc-hr-invisible-small"> </div><?php

				$price_suffix = dttheme_option("property","currency");
				$price_suffix = dt_wp_kses( $price_suffix );

				$areaunit = dttheme_option("property","area_unit");
				$areaunit = dt_wp_kses( $areaunit ); 

				$columns = 3; 


				$layout = get_post_meta ( $post->ID, "_assigned_property_layout",true);
				if( $layout === "dt-sc-one-half" ){
					$post_class = $show_sidebar ? " column dt-sc-one-half with-sidebar" : " column dt-sc-one-half";
					$columns = 2;
				}else{
					$post_class = $show_sidebar ? " column dt-sc-one-third with-sidebar" : " column dt-sc-one-third";
					$columns = 3;
				}

				$args = array(
					'post_type'=>'dt_properties',
					'posts_per_page'=>-1,
					'order_by'=> 'published',
					'meta_query' => array(array(
						'key'     => '_property_agent',
						'value'   => $post->ID,
						'compare' => 'LIKE',)
				));

				$query = new WP_Query($args);
				
				if( $query->have_posts() ):
					$i = 1;

					while( $query->have_posts() ):
						$query->the_post();

						$temp_class = "";
						$temp_class = ( $i == 1 ) ? "{$post_class} first" : $post_class;
						$i = ( $i == $columns ) ? 1 : $i+1;

						$the_id = get_the_ID();
						$permalink = get_permalink($the_id);
						$title = get_the_title($the_id);

						$contract_type = "";
						$contract_type_slug = "";
						$contract = get_the_terms( $the_id, 'contract_type' );
						if( is_object( $contract) || is_array($contract) ){
							foreach ( $contract as $c ) :
								$contract_type = $c->name;
								$contract_type_slug = $c->slug;
							endforeach;
						}

						$property_type = "";
						$property_type = get_the_terms( $the_id, 'property_type' );
						if( is_object( $property_type) || is_array($property_type) ){
							foreach ( $property_type as $c ) :
								$property_type = $c->name;
							endforeach;
						}

						$media = get_post_meta ( $the_id, "_property_media",true);
						$media = is_array($media) ? $media : array();

						$price = get_post_meta ( $the_id, "_property_price",true);
						$price = dt_wp_kses( $price );?>

							<!-- Property Item -->
							<div class="column <?php echo esc_attr($temp_class);?>">

								<div class="property-item">

									<div class="property-thumb">
										<?php if( !empty( $contract_type ) ):?>
												<span class="property-contract-type <?php echo esc_attr($contract_type_slug);?>">
													<?php echo esc_html($contract_type);?>
												</span>
										<?php endif;?>
										<ul class="porperty-slider"><?php
											if( array_key_exists("items_name",$media) ) {
												foreach( $media["items_name"] as $key => $item ) {

													$current_item = $media["items"][$key];

													if( "video" === $item ) {
														echo "<li>".wp_oembed_get( $current_item )."</li>";
													} else {
														echo "<li><a href='".esc_url($permalink)."'><img src='".esc_url($current_item)."' alt='' title='' /></a></li>";
													}
												}
											} else {
												echo "<li><a href='".esc_url($permalink)."'><img src='http://placehold.it/1060x718&text=Real Home' alt='' title=''/></a></li>";										
											}?>
										</ul>

										<div class="property-thumb-meta"><?php
											if( !empty($property_type) ) 
												echo "<span class='property-type'>".esc_html($property_type)."</span>";

											if( !empty($price) )
												echo "<span class='property-price'>{$price_suffix} {$price} </span>";
										?></div>
									</div>

									<div class="property-details">
										<div class="property-details-inner">
											<h2><a href='<?php echo esc_url($permalink);?>'><?php echo esc_html($title);?></a></h2>
											<h3><?php $address = get_post_meta ( $the_id, "_property_address",true);
													$address = dt_wp_kses( $address );
													echo $address;?></h3>

											<ul class="property-meta"><?php
												$area = get_post_meta ( $the_id, "_area",true);
												$area = dt_wp_kses( $area );

												$bedrooms = get_post_meta ( $the_id, "_bedrooms",true);
												$bedrooms = dt_wp_kses( $bedrooms );

												$bathrooms = get_post_meta ( $the_id, "_bathrooms",true);
												$bathrooms = dt_wp_kses( $bathrooms );

												$floors = get_post_meta ( $the_id, "_floors",true);
												$floors = dt_wp_kses( $floors );

												$parking = get_post_meta ( $the_id, "_parking",true);
												$parking = dt_wp_kses( $parking );

												if( !empty($area) )
													echo "<li>{$area}{$areaunit}<span>".__('Area','dt_themes').'</span></li>';

												if( !empty($bedrooms) )
													echo "<li>{$bedrooms}<span>".__('Beds','dt_themes')."</span></li>";

												if( !empty($bathrooms) )
													echo "<li>{$bathrooms}<span>".__('Baths','dt_themes')."</span></li>";

												if( !empty($floors) )
													echo "<li>{$floors}<span>".__('Floors','dt_themes')."</span></li>";

												if( !empty($parking) )
													echo "<li>{$parking}<span>".__('Garages','dt_themes')."</span></li>";?>
											</ul>
										</div>
									</div>

								</div>
							</div><!-- Property Item End--><?php
					endwhile;
				endif;
		endif;?>
	</section><!-- ** Primary Section End ** --><?php
	if ( $show_sidebar ):
		if ( $show_right_sidebar ):?>
			<!-- Secondary Right -->
			<section id="secondary-right" class="secondary-sidebar <?php echo esc_attr($sidebar_class);?>">
				<?php get_sidebar( 'right' );?>
			</section><?php
		endif;
	endif;?>
<?php get_footer(); ?>