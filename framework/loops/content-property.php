<?php $the_id = get_the_id();

	$permalink = get_permalink($the_id);

	$contract_type = "";
	$contract_type_slug = "";
	$contract_type_link = "";
	$contract = get_the_terms( $the_id, 'contract_type' );
	if( is_object( $contract) || is_array($contract) ){
		foreach ( $contract as $c ) :
			$contract_type = $c->name;
			$contract_type_slug = $c->slug;
			$contract_type_link = get_term_link( $contract_type_slug, 'contract_type' );
		endforeach;
	}
	
	$property_type = "";
	$property_type_link = "";
	$property_type = get_the_terms( $the_id, 'property_type' );
	if( is_object( $property_type) || is_array($property_type) ){
		foreach ( $property_type as $c ) :
			$property_type = $c->name;
			$property_type_link = get_term_link( $c->slug, 'property_type' );
		endforeach;
	}
	
	$media = get_post_meta ( $the_id, "_property_media",true);
	$media = is_array($media) ? $media : array();
	
	$price = get_post_meta ( $the_id, "_property_price",true); ?>
<div class="property-item">
	<div class="property-thumb"><?php
		if( !empty( $contract_type ) ):?>
        	<span class="property-contract-type <?php echo esc_attr( $contract_type_slug );?>">
            	<?php echo"<a href='".esc_url( $contract_type_link )."'>".esc_html( $contract_type )."</a>";?></span><?php
		endif;?>
        
        <ul class="porperty-slider"><?php
			if( array_key_exists("items_name",$media) ) {
				foreach( $media["items_name"] as $key => $item ) {
					
					$current_item = $media["items"][$key];
					
					if( "video" === $item ) {
						echo "<li>".wp_oembed_get( $current_item )."</li>";
					} else {
						echo "<li> <a href='{$permalink}'> <img src='".esc_url( $current_item )."' alt=''/></a></li>";
					}
				}
			} else {
				echo "<li> <a href='{$permalink}'> <img src='http://placehold.it/1060x718&text=Real Home' alt=''/></a></li>";
			}
		?></ul>
        
        <div class="property-thumb-meta"><?php
			if( !empty($property_type) ) 
				echo "<span class='property-type'><a href='".esc_url( $property_type_link )."'>".esc_html($property_type)."</a></span>";
			
			if( !empty($price) )
				echo "<span class='property-price'>{$price_suffix} {$price} </span>";?>
        </div>        
    </div>
    
    <div class="property-details">
    	<div class="property-details-inner">
            <?php the_title( sprintf( '<h2><a href="%s">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
            <h3><?php $address = get_post_meta ( $the_id, "_property_address",true);
				$address = dt_wp_kses( $address );
				echo $address; ?></h3>
            <ul class="property-meta"><?php
				$area = get_post_meta ( $the_id, "_area",true);
				$area = dt_wp_kses( $area );
				
				$bedrooms = get_post_meta ( $the_id, "_bedrooms",true);
				$bedrooms = dt_wp_kses( $bedrooms );
				
				$bathrooms = get_post_meta ( $the_id, "_bathrooms",true);
				$bathrooms = dt_wp_kses( $bathrooms );
				
				$floors = get_post_meta ( $the_id, "_floors",true);
				$floors = dt_wp_kses( $floors );
				
				$parking = get_post_meta ( $the_id, "_parking",true);
				$parking = dt_wp_kses( $parking );
				
				if( !empty($area) )
					echo "<li>{$area}{$areaunit}<span>".__('Area','dt_themes').'</span></li>';
				
				if( !empty($bedrooms) )
					echo "<li>{$bedrooms}<span>".__('Beds','dt_themes')."</span></li>";
					
				if( !empty($bathrooms) )
					echo "<li>{$bathrooms}<span>".__('Baths','dt_themes')."</span></li>";
					
				if( !empty($floors) )
					echo "<li>{$floors}<span>".__('Floors','dt_themes')."</span></li>";
					
				if( !empty($parking) )
					echo "<li>{$parking}<span>".__('Garages','dt_themes')."</span></li>";?>
            </ul>
        </div>
    </div>
</div>