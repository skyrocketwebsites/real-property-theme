<?php require_once('../../../../wp-load.php');
	require_once('../../../../wp-includes/class-phpmailer.php');
	
	$message = "";
	$message .=  isset($_POST['txtfname'])  && !empty($_POST['txtfname']) ? __('First Name','dt_themes')." - ".$_POST['txtfname']."\n" : "";
	$message .=  isset($_POST['txtlname']) && !empty($_POST['txtlname']) ? __('Last Name','dt_themes')." - ".$_POST['txtlname']."\n": "";
	$message .=  isset($_POST['txtemail']) && !empty($_POST['txtemail']) ? __('Email','dt_themes')." - ".$_POST['txtemail']."\n": "";
	$message .=  isset($_POST['phone']) && !empty($_POST['phone']) ? __( "Phone No","dt_themes")." - ".$_POST['phone']."\n" : "";
	$message .=  isset($_POST['message']) && !empty($_POST['message']) ? __( "Message","dt_themes")." - ".$_POST['message']."\n":"";
	$message .= __("Property link","dt_themes")." - ".$_POST['property_link']."\n";
	
	$header  = 'From :'.$_POST['txtemail']."\r\n";
	$subject = __(" Enquire For Property ","dt_themes")." - ".$_POST['txtproperty'];

	if( isset( $_POST['admin_emailid'])  ){
		$is_sent = wp_mail( $_POST['admin_emailid'], $subject,$message,$header);
	}

	if( isset( $_POST['agents']) ) {
		foreach( $_POST['agents'] as $agent ) {
			wp_mail( $agent, $subject,$message,$header);
		}	
	}

	if( $is_sent ){
		echo "<div class='dt-sc-success-box'>".__('Your enquire was successfully sent','dt_themes')."</div>";
	} else {
		echo "<div class='dt-sc-error-box'>".__('An error occurred. Your enquire can not be sent.','dt_themes')."</div>";
	}
?>