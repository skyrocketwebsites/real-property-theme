<?php
/** MY Property Widget Widget
  * Objective:
  *		1.To list out portfolio items
**/
class MY_Property_Widget extends WP_Widget {
	#1.constructor
	function MY_Property_Widget() {
		$widget_options = array("classname"=>'widget_popular_entries', 'description'=>'To list out recent property items');
		parent::__construct(false,IAMD_THEME_NAME.__(' Property','dt_themes'),$widget_options);
	}
	
	#2.widget input form in back-end
	function form($instance) {
		$instance = wp_parse_args( (array) $instance,array('title'=>'','_post_count'=>'','_property_type'=>'') );
		$title = strip_tags($instance['title']);
		$_post_count = !empty($instance['_post_count']) ? strip_tags($instance['_post_count']) : "-1";
		$_property_type = !empty($instance['_property_type']) ? $instance['_property_type']: array();?>
        
        <!-- Form -->
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','dt_themes');?> 
		   <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" 
            type="text" value="<?php echo esc_attr($title); ?>" /></label></p>
           
	    <p><label for="<?php echo $this->get_field_id('_property_type'); ?>">
			<?php _e('Choose the categories you want to display (multiple selection possible)','dt_themes');?></label>
            <select class="widefat" id="<?php echo $this->get_field_id('_property_type').'[]';?>" 
            	name="<?php echo $this->get_field_name('_property_type').'[]';?>" multiple="multiple">
                <option value=""><?php _e("Select",'dt_themes');?></option>
           	<?php $cats = get_categories('taxonomy=property_type&hide_empty=1');
			foreach ($cats as $cat):
				$id = esc_attr($cat->term_id);
				$selected = ( in_array($id,$_property_type)) ? 'selected="selected"' : '';
				$title = esc_html($cat->name);
				echo "<option value='{$id}' {$selected}>{$title}</option>";
			endforeach;?>
            </select></p>

	    <p><label for="<?php echo $this->get_field_id('_post_count'); ?>"><?php _e('No.of posts to show:','dt_themes');?></label>
		   <input id="<?php echo $this->get_field_id('_post_count'); ?>" name="<?php echo $this->get_field_name('_post_count'); ?>" value="<?php echo $_post_count?>" /></p>
        <!-- Form end-->
<?php
	}
	#3.processes & saves the twitter widget option
	function update( $new_instance,$old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['_post_count'] = strip_tags($new_instance['_post_count']);
		$instance['_property_type'] = $new_instance['_property_type'];
	return $instance;
	}
	
	#4.output in front-end
	function widget($args, $instance) {
		extract($args);

		global $post;

		$title = empty($instance['title']) ?'' : apply_filters('widget_title', $instance['title']);
		$_post_count = (int) $instance['_post_count'];
		$_property_type = "";

		if(!empty($instance['_property_type']) && is_array($instance['_property_type'])):
			$_property_type =  array_filter($instance['_property_type']);
		elseif(!empty($instance['_property_type'])):
			$_property_type = explode(",",$instance['_property_type']);
		endif;

		$price_suffix = dttheme_option("property","currency");


		$arg = array('posts_per_page' => $_post_count ,'post_type' => 'dt_properties');
		$arg = empty($_property_type) ? $arg : array(
					'post_type' => 'dt_properties',
					'posts_per_page'=> $_post_count,
					'tax_query'		=> array(array( 'taxonomy'=>'property_type', 'field'=>'id', 'operator'=>'IN', 'terms'=>$_property_type ) ));



		echo $before_widget;
 	    echo $before_title.$title.$after_title;
		echo "<div class='recent-property-widget'><ul>";		
			 query_posts($arg);
			 if( have_posts()) :
			 while(have_posts()):
			 	the_post();

			 	$the_id = get_the_ID();
			 	$price = get_post_meta ( $the_id, "_property_price",true);

				$media = get_post_meta ( $the_id, "_property_media",true);
				$media = is_array($media) ? $media : array();
				
				
				$img = "http://placehold.it/1060x718&text=Real Home";
				if( array_key_exists("items_id", $media) ) {
					$items = array_unique( $media['items_id']);
					if( array_key_exists("video",$items)){
						unset( $items[array_search("video", $items)]);
					}
					$img = $media['items_thumbnail'][key($items)];
				}
				


				$title = ( strlen(get_the_title()) > 50 ) ? substr(get_the_title(),0,40)."..." :get_the_title();
				echo "<li>";
					echo "<a href='".get_permalink()."' class='thumb'>";
					echo "<img src='{$img}' alt='{$title}'/>";
					echo "</a>";

					echo "<h6><a href='".get_permalink()."'>{$title}</a></h6>";
					if( !empty($price) )
						echo "<span class='property-price'>{$price_suffix} {$price} </span>";
				echo "</li>";
			 endwhile;
			 else:
			 	echo "<li>".__('No Properties found','dt_themes')."</li>";
			 endif;
			 wp_reset_query();
	 	echo "</ul></div>";			 
		echo $after_widget;
	}
}?>