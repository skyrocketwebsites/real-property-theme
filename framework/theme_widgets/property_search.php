<?php
class MY_Property_Search_Widget extends WP_Widget {
	#1.constructor
	function MY_Property_Search_Widget() {
		
		$widget_options = array("classname"=>'widget_properties_search', 'description'=>__('Shows Property Search Form','dt_themes'));
		parent::__construct(false,IAMD_THEME_NAME.__(' Search','dt_themes'),$widget_options);
	}

	#2.widget input form in back-end
	function form($instance) {
		$instance = wp_parse_args( (array) $instance, array(  'title'=>'', 'contract_type'=>'', 'property_type'=>'', 'beds_module' => '', 'baths_module' => '', 'floors_module' => '',
		'garages_module'=>'','price_module' => '', 'location_module' =>'') );
		
		$title = strip_tags($instance['title']);
		$contract_type = $instance['contract_type'];
		$property_type = !empty( $instance['property_type'] ) ? $instance['property_type'] : array();
		$beds_module = $instance['beds_module'];
		$location_module = $instance['location_module'];
		$baths_module = $instance['baths_module'];
		$floors_module = $instance['floors_module'];
		$garages_module = $instance['garages_module'];
		$price_module = $instance['price_module'];?>
    	<!-- Form -->
        	<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:','dt_themes');?>
            <input class="widefat" id="<?php echo $this->get_field_id('title');?>" name="<?php echo $this->get_field_name('title');?>" 
            	type="text" value="<?php echo esc_attr($title);?>"/></label></p>
            
            <p><label for="<?php echo $this->get_field_id('contract_type');?>"><?php _e('Contract Type','dt_themes');?></label>
            	<select class="widefat" id="<?php echo $this->get_field_id('contract_type').'[]';?>"  name="<?php echo $this->get_field_name('contract_type');?>">
	                <option value="default"><?php _e("All",'dt_themes');?></option><?php
                	$cats = get_categories('taxonomy=contract_type&hide_empty=1');
					foreach ($cats as $cat):
						$id = esc_attr($cat->slug);
						$selected = ( $id  == $contract_type ) ? 'selected="selected"' : '';
						$title = esc_html($cat->name);
						echo "<option value='{$id}' {$selected}>{$title}</option>";
					endforeach;?>
                </select>
            </p>
            
            <p><label for="<?php echo $this->get_field_id('property_type'); ?>"><?php _e('Property Type','dt_themes');?></label>
            <select class="widefat" id="<?php echo $this->get_field_id('property_type').'[]';?>"  name="<?php echo $this->get_field_name('property_type').'[]';?>"
            	multiple="multiple"><?php
				$cats = get_categories('taxonomy=property_type&hide_empty=1');
				foreach ($cats as $cat):
					$id = esc_attr($cat->term_id);
					$selected = ( in_array($id,$property_type)) ? ' selected="selected" ' : '';
					$title = esc_html($cat->name);
					echo "<option value='{$id}' {$selected}>{$title}</option>";
				endforeach;?>
            </select></p>
        
        	<p><label for="<?php echo $this->get_field_id('location_module');?>"><?php _e('Enable Locations Module','dt_themes');?></label>
            <select class="widefat" id="<?php echo $this->get_field_id('location_module');?>" name="<?php echo $this->get_field_name('location_module');?>"><?php
				$options = array("true" => "Yes", "false" => "No");
				foreach( $options as $k => $v ):
					$id = esc_attr($k);
					$title = esc_html($v);
					$selected = ( $id == $location_module ) ? ' selected="selected" ':'';
					echo "<option value='{$id}' {$selected}>{$title}</option>";
				endforeach;?>
            </select></p>
        
        	<p><label for="<?php echo $this->get_field_id('beds_module');?>"><?php _e('Enable Beds Module','dt_themes');?></label>
            <select class="widefat" id="<?php echo $this->get_field_id('beds_module');?>" name="<?php echo $this->get_field_name('beds_module');?>"><?php
				$options = array("true" => "Yes", "false" => "No");
				foreach( $options as $k => $v ):
					$id = esc_attr($k);
					$title = esc_html($v);
					$selected = ( $id == $beds_module ) ? ' selected="selected" ':'';
					echo "<option value='{$id}' {$selected}>{$title}</option>";
				endforeach;?>
            </select></p>

        	<p><label for="<?php echo $this->get_field_id('baths_module');?>"><?php _e('Enable Baths Module','dt_themes');?></label>
            <select class="widefat" id="<?php echo $this->get_field_id('baths_module');?>" name="<?php echo $this->get_field_name('baths_module');?>"><?php
				$options = array("true" => "Yes", "false" => "No");
				foreach( $options as $k => $v ):
					$id = esc_attr($k);
					$title = esc_html($v);
					$selected = ( $id == $baths_module ) ? ' selected="selected" ':'';
					echo "<option value='{$id}' {$selected}>{$title}</option>";
				endforeach;?>
            </select></p>

        	<p><label for="<?php echo $this->get_field_id('floors_module');?>"><?php _e('Enable Floors Module','dt_themes');?></label>
            <select class="widefat" id="<?php echo $this->get_field_id('floors_module');?>" name="<?php echo $this->get_field_name('floors_module');?>"><?php
				$options = array("true" => "Yes", "false" => "No");
				foreach( $options as $k => $v ):
					$id = esc_attr($k);
					$title = esc_html($v);
					$selected = (  $id == $floors_module ) ? ' selected="selected" ':'';
					echo "<option value='{$id}' {$selected}>{$title}</option>";
				endforeach;?>
            </select></p>

        	<p><label for="<?php echo $this->get_field_id('garages_module');?>"><?php _e('Enable Garages Module','dt_themes');?></label>
            <select class="widefat" id="<?php echo $this->get_field_id('garages_module');?>" name="<?php echo $this->get_field_name('garages_module');?>"><?php
				$options = array("true" => "Yes", "false" => "No");
				foreach( $options as $k => $v ):
					$id = esc_attr($k);
					$title = esc_html($v);
					$selected = ( $id == $garages_module ) ? ' selected="selected" ':'';
					echo "<option value='{$id}' {$selected}>{$title}</option>";
				endforeach;?>
            </select></p>

        	<p><label for="<?php echo $this->get_field_id('price_module');?>"><?php _e('Enable Price Module','dt_themes');?></label>
            <select class="widefat" id="<?php echo $this->get_field_id('price_module');?>" name="<?php echo $this->get_field_name('price_module');?>"><?php
				$options = array("true" => "Yes", "false" => "No");
				foreach( $options as $k => $v ):
					$id = esc_attr($k);
					$title = esc_html($v);
					$selected = ( $id == $price_module ) ? ' selected="selected" ':'';
					echo "<option value='{$id}' {$selected}>{$title}</option>";
				endforeach;?>
            </select></p>
        <!-- Form -->
	<?php
	}

	#3.processes & saves the twitter widget option
	function update( $new_instance,$old_instance ) {
		$instance = $old_instance;
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['contract_type'] = $new_instance['contract_type'];
		$instance['property_type'] = empty( $new_instance['property_type'] ) ? array() : $new_instance['property_type'];
		$instance['location_module'] = $new_instance['location_module'];
		$instance['beds_module'] = $new_instance['beds_module'];
		$instance['baths_module'] = $new_instance['baths_module'];
		$instance['garages_module'] = $new_instance['garages_module'];
		$instance['floors_module'] = $new_instance['floors_module'];
		$instance['price_module'] = $new_instance['price_module'];		
	return $instance;
	}

	#4.output in front-end
	function widget($args, $instance) {
		extract($args);
		$title = empty($instance['title']) ? '' : apply_filters('widget_title', $instance['title']);
		echo $before_widget;
		
		if( !empty($title))
 	    echo $before_title.$title.$after_title;
		
		$property = dttheme_option("property");
		
		$action = dt_get_page_permalink_by_its_template('tpl-property-search.php');
		$key = $instance['contract_type'];
		
		echo "<form action='{$action}' method='get'>";
		echo "<input type='hidden' name='searchby' value='{$key}'>";
	
		#Property Type Module
		if( !empty( $instance['property_type'] ) ):
			if( count($instance['property_type']) == 1  ) {
				echo "<input type='hidden' name='ptype' value='{$instance['property_type'][0]}'>";
			} else {
				echo '<div class="property-type-module medium-module">';
				echo '<label>'.__('Type','dt_themes').'</label>';
				echo '<select name="ptype">';
					foreach( $instance['property_type'] as $term_id ) {
						$id =  esc_attr( $term_id );
						$term = get_term( $term_id, 'property_type' );
						$title = esc_html ( $term->name );
						echo  "<option value='{$id}'>{$title}</option>";
					}
				echo '</select>';
				echo '</div>';
			}
		endif;
		
		#Location Module
		if( $instance['location_module'] == 'true' ):
			echo '<div class="location-module medium-module">';
			echo '<label>'.__('Locations','dt_themes').'</label>';
			echo '<select name="plocation">';
			echo '<option value="0">'.__("Location","dt_themes").'</option>';
				$property_locations = get_categories("taxonomy=property_location&hide_empty=1");
				foreach ( $property_locations as $property_location ) {
					$id = esc_attr( $property_location->term_id );
					$title = esc_html( $property_location->name );
					$selected = "";
					echo  "<option value='{$id}' {$selected} >{$title}</option>";
				}
				
			echo '</select>';
			echo '</div>';
        endif;
		 
		 
		#Min price Module
		if( $instance['price_module'] == 'true' ):
		 	echo '<div class="min-price-module medium-module">';
			echo '<label>'.__('Min Price','dt_themes').'</label>';
			echo '<select name="minprice">';
			echo '<option value="0">'.__("Any","dt_themes").'</option>';
				$min_prices = array_key_exists("min-price-for-{$key}", $property) ? $property["min-price-for-{$key}"] : array();
				$min_prices = array_filter($min_prices);
				$min_prices = array_unique($min_prices);
				foreach ( $min_prices as $min_price ) {
					$selected = "";
					echo  "<option value='{$min_price}' {$selected} >{$min_price}</option>";
                }
			echo '</select>';
			echo '</div>';
        endif;
		  
		#Max price Module
		if( $instance['price_module'] == 'true' ):
		  	echo '<div class="max-price-module medium-module">';
			echo '<label>'.__('Max Price','dt_themes').'</label>';
			echo '<select name="maxprice">';
			echo '<option value="0">'.__("Any","dt_themes").'</option>';
				$max_prices = array_key_exists("max-price-for-{$key}", $property) ? $property["max-price-for-{$key}"] : array();
				$max_prices = array_filter($max_prices);
				$max_prices = array_unique($max_prices);
				foreach ( $max_prices as $max_price ) {
					$selected = "";
					echo  "<option value='{$max_price}' {$selected} >{$max_price}</option>";
				}
			echo '</select>';
			echo '</div>';
        endif;
		  
		#Bed
		if( $instance['beds_module'] == 'true' ):
		 	echo '<div class="beds-module small-module">';
			echo '<label>'.__('Beds','dt_themes').'</label>';
				echo '<select name="pbeds">';
				echo '<option value=">0">'.__("Any","dt_themes").'</option>';
					$beds = array_key_exists("beds-for-{$key}", $property) ? $property["beds-for-{$key}"]: array();
					$beds = array_filter($beds);
					$beds = array_unique( $beds);
					
					foreach ( $beds as $bed ) {
						$selected = "";
						echo  "<option value='{$bed}' {$selected} >{$bed}</option>";
					}
				echo '</select>';
			echo '</div>';
        endif;

		#Bath
		if( $instance['baths_module'] == 'true' ):
			echo '<div class="baths-module small-module">';
			echo '<label>'.__('Baths','dt_themes').'</label>';
			echo '<select name="pbaths">';
			echo '<option value=">0">'.__("Any","dt_themes").'</option>';
					$baths = array_key_exists("baths-for-{$key}", $property) ? $property["baths-for-{$key}"]: array();
					$baths = array_filter($baths);
					$baths = array_unique( $baths);
					foreach ( $baths as $bath ) {
						$selected = "";
						echo  "<option value='{$bath}' {$selected} >{$bath}</option>";
					}        
			echo '</select>';
			echo '</div>';
		endif;
		
		#Floors
		if( $instance['floors_module'] == 'true' ):
			echo '<div class="floors-module small-module">';
			echo '<label>'.__('Floors','dt_themes').'</label>';
			echo '<select name="pfloors">';
			echo '<option value=">0">'.__("Any","dt_themes").'</option>';
				$floors = array_key_exists("floors-for-{$key}", $property) ? $property["floors-for-{$key}"]: array();
				$floors = array_filter($floors);
				$floors = array_unique( $floors);
				foreach ( $floors as $floor ) {
					$selected = "";
					echo  "<option value='{$floor}' {$selected} >{$floor}</option>";
				}
			echo '</select>';
			echo '</div>';
        endif;
		
		#Garages
		if( $instance['garages_module'] == 'true' ):
			echo '<div class="garages-module small-module">';
			echo '<label>'.__('Garages','dt_themes').'</label>';
			echo '<select name="pgarages">';
			echo '<option value=">0">'.__("Any","dt_themes").'</option>';
				$garages = array_key_exists("garages-for-{$key}", $property) ? $property["garages-for-{$key}"]: array();
				$garages = array_filter($garages);
				$garages = array_unique( $garages);
				foreach ( $garages as $garage ) {
					$selected = "";
					echo  "<option value='{$garage}' {$selected} >{$garage}</option>";
				}
			echo '</select>';
			echo '</div>';
        endif;                    
		   
		echo '<input type="submit" name="dt-propery-search-submit" value="'.__("Search","dt_themes").'" />';
		echo '</form>';
		echo $after_widget;
	}
}?>