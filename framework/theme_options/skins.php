<!-- #skin -->
<div id="skin" class="bpanel-content">
    <!-- .bpanel-main-content -->
    <div class="bpanel-main-content">
        <ul class="sub-panel">
			<?php $sub_menus = array(
					array("title"=>__("Skins",'dt_themes'), "link"=>"#appearance-skins"),
					array("title"=>__("Custom",'dt_themes'),"link"=>"#appearance-custom-skin"));
						
				  foreach($sub_menus as $menu): ?>
                  	<li><a href="<?php echo $menu['link']?>"><?php echo $menu['title'];?></a></li>
			<?php endforeach?>
        </ul>

        <!--Skins Section -->
        <div id="appearance-skins" class="tab-content">
	        <div class="bpanel-box">
                <div class="box-title"><h3><?php _e('Current Skin','dt_themes');?></h3></div>
                <div class="box-content">
	                <?php $theme = dttheme_option('appearance','skin'); ?>
                	 <ul id="j-current-theme-container" class="skins-list" dummy-content="<?php echo $theme.'-dummy';?>">
                     	 <li data-attachment-theme="<?php echo $theme;?>">
                            <img src="<?php echo IAMD_BASE_URL."skins/{$theme}/screenshot.png";?>" alt='' width='250' height='180' />
                            <input type="hidden" name="mytheme[appearance][skin]" value="<?php echo $theme;?>" />
                            <h4><?php echo $theme;?></h4>
                        </li>
                     </ul>
                </div>
                
                <div class="box-title"><h3><?php _e('Available Skins','dt_themes');?></h3></div>
                <div class="box-content">
                	<ul id="j-available-themes" class="skins-list">
					<?php foreach(getFolders(get_template_directory()."/skins") as $skin ):
                            $active = ($theme == $skin) ? 'class="active"' : NULL;
                            $img = IAMD_BASE_URL."skins/{$skin}/screenshot.png";
                            echo "<li data-attachment-theme='{$skin}' {$active}>";
                            echo "<img src='{$img}' alt='' width='250' height='180' />";
                            echo "<h4>{$skin}</h4>";
                            echo "</li>";
                         endforeach;?>
                    </ul>
                </div>
            </div>
        </div><!--Skins Section  End-->
        
        <!--Custom Skin Section  End-->
        <div id="appearance-custom-skin" class="tab-content">
	        <div class="bpanel-box">
                <div class="box-title"><h3><?php _e('Custom Skin','dt_themes');?></h3></div>
                <div class="box-content">
                
                    <div class="bpanel-option-set">
                    	<h6><?php _e('Disable Custom Skin','dt_themes');?></h6>
                        <?php dttheme_switch("",'appearance','disable-custom-skin');?>
                        <p class="note"> <?php _e('Enable or Disable custom skin colors.','dt_themes');?>  </p>                        
                    </div>
                    
                    <div class="clear"> </div>
                    
                    <div class="column one-half">
                    	<?php $label = 		__("Primary Color",'dt_themes');
                          $name  =		"mytheme[appearance][skin-primary-color]";	
						  $value =  	(dttheme_option('appearance','skin-primary-color') != NULL) ? dttheme_option('appearance','skin-primary-color') : "#";
                          $tooltip = 	__("Pick a custom primary color e.g. #564912",'dt_themes'); ?>
                          <h6><?php echo $label;?></h6>	
                          <?php dttheme_admin_color_picker("",$name,$value,'');?>  
                          <p class="note"><?php echo $tooltip;?></p>
                          <div class="clear"></div>
                   </div>
                   
                   <div class="column one-half last">
                    	<?php $label = 		__("Secondary Color",'dt_themes');
                          $name  =		"mytheme[appearance][skin-secondary-color]";	
						  $value =  	(dttheme_option('appearance','skin-secondary-color') != NULL) ? dttheme_option('appearance','skin-secondary-color') : "#";
                          $tooltip = 	__("Pick a custom secondary color e.g. #564912",'dt_themes'); ?>
                          <h6><?php echo $label;?></h6>	
                          <?php dttheme_admin_color_picker("",$name,$value,'');?>  
                          <p class="note"><?php echo $tooltip;?></p>
                          <div class="clear"></div>
                   </div>       
               </div>
            </div>   
        </div><!--Custom Skin Section  End-->

    </div><!-- .bpanel-main-content end -->
</div><!-- #skin  end-->