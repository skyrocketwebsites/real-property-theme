<!-- #widgetarea -->
<div id="widgetarea" class="bpanel-content">
    <!-- .bpanel-main-content -->
    <div class="bpanel-main-content">
        <ul class="sub-panel widget-area-nav">
            <li><a href="#for-pages"><?php _e("Pages",'dt_themes');?></a></li>
            <li><a href="#for-posts"><?php _e("Posts",'dt_themes');?></a></li>
            <li><a href="#for-categories"><?php _e("Categories",'dt_themes');?></a></li>
            <li><a href="#for-dt_properties"><?php _e("Properties",'dt_themes');?></a></li>
            <li><a href="#for-dt_agents"><?php _e("Agents",'dt_themes');?></a></li>
	        <li><a href="#for-dt_agencies"><?php _e("Agencies",'dt_themes');?></a></li>
        </ul>
        
        <!-- #for-pages -->
        <div id="for-pages" class="tab-content">
        	<!-- .bpanel-box-->
        	<div class="bpanel-box">
            	<div class="box-title">
	                <h3><?php _e('Add LEFT SIDE widget areas','dt_themes');?></h3>
                </div>
                <!-- .box-content -->
                <div class="box-content">
                	<p class="note"><?php _e("Select a PAGE that should receive a new LEFT SIDE widget area:",'dt_themes');?></p>

                	<?php if(is_array(dttheme_option("widgetarea","left-pages"))):
							$pages = array_unique(dttheme_option("widgetarea","left-pages"));
							foreach($pages as $page):
							  echo "<!-- Category Drop Down Container -->";
							  echo "<div class='multidropdown'>";
							  echo  dttheme_pagelist("widgetarea,left-pages",$page,"multidropdown");						  
							  echo "</div><!-- Category Drop Down Container end-->";
							endforeach;
						  else:	
							  echo "<!-- Pages Drop Down Container -->";
							  echo "<div class='multidropdown'>";
							  echo  dttheme_pagelist("widgetarea,left-pages","","multidropdown");						  
							  echo "</div><!-- Category Drop Down Container end-->";
						  endif;?>
                </div><!-- .box-content End-->
            </div><!-- .bpanel-box -->


            <!-- .bpanel-box-->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php _e('Add RIGHT SIDE widget areas','dt_themes');?></h3>
                </div>
                <!-- .box-content -->
                <div class="box-content">
                    <p class="note"><?php _e("Select a PAGE that should receive a new RIGHT SIDE widget area:",'dt_themes');?></p>

                    <?php if(is_array(dttheme_option("widgetarea","right-pages"))):
                            $pages = array_unique(dttheme_option("widgetarea","right-pages"));
                            foreach($pages as $page):
                              echo "<!-- Category Drop Down Container -->";
                              echo "<div class='multidropdown'>";
                              echo  dttheme_pagelist("widgetarea,right-pages",$page,"multidropdown");                       
                              echo "</div><!-- Category Drop Down Container end-->";
                            endforeach;
                          else: 
                              echo "<!-- Pages Drop Down Container -->";
                              echo "<div class='multidropdown'>";
                              echo  dttheme_pagelist("widgetarea,right-pages","","multidropdown");                          
                              echo "</div><!-- Category Drop Down Container end-->";
                          endif;?>
                </div><!-- .box-content End-->
            </div><!-- .bpanel-box -->

        </div><!-- #for-pages end-->


        <!-- #for-posts -->
        <div id="for-posts" class="tab-content">
        	<!-- .bpanel-box-->
        	<div class="bpanel-box">
            	<div class="box-title">
	                <h3><?php _e('Add LEFT SIDE widget areas','dt_themes');?></h3>
                </div>
                <!-- .box-content -->
                <div class="box-content">
                	<p class="note"><?php _e("Select a POST that should receive a new LEFT SIDE widget area:",'dt_themes');?></p>
                	<?php if(is_array(dttheme_option("widgetarea","left-posts"))):
							$posts = array_unique(dttheme_option("widgetarea","left-posts"));
							foreach($posts as $post):
							  echo "<!-- Category Drop Down Container -->";
							  echo "<div class='multidropdown'>";
							  echo  dttheme_postlist("widgetarea,left-posts",$post,"multidropdown");						  
							  echo "</div><!-- Category Drop Down Container end-->";
							endforeach;
						  else:
							 echo "<!-- Post Drop Down Container -->";
							 echo "<div class='multidropdown'>";
							 echo  dttheme_postlist("widgetarea,left-posts","","multidropdown");						  
							 echo "</div><!-- Category Drop Down Container end-->";						  
						  endif;?>
                </div><!-- .box-content End-->
            </div><!-- .bpanel-box -->

            <!-- .bpanel-box-->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php _e('Add RIGHT SIDE widget areas','dt_themes');?></h3>
                </div>
                <!-- .box-content -->
                <div class="box-content">
                    <p class="note"><?php _e("Select a POST that should receive a new RIGHT SIDE widget area:",'dt_themes');?></p>
                    <?php if(is_array(dttheme_option("widgetarea","right-posts"))):
                            $posts = array_unique(dttheme_option("widgetarea","right-posts"));
                            foreach($posts as $post):
                              echo "<!-- Category Drop Down Container -->";
                              echo "<div class='multidropdown'>";
                              echo  dttheme_postlist("widgetarea,right-posts",$post,"multidropdown");                       
                              echo "</div><!-- Category Drop Down Container end-->";
                            endforeach;
                          else:
                             echo "<!-- Post Drop Down Container -->";
                             echo "<div class='multidropdown'>";
                             echo  dttheme_postlist("widgetarea,right-posts","","multidropdown");                       
                             echo "</div><!-- Category Drop Down Container end-->";                       
                          endif;?>
                </div><!-- .box-content End-->
            </div><!-- .bpanel-box -->

        </div><!-- #for-posts end-->

        <!-- #for-categories -->
        <div id="for-categories" class="tab-content">
        	<!-- .bpanel-box-->
        	<div class="bpanel-box">
            	<div class="box-title">
	                <h3><?php _e('Add LEFT SIDE widget areas','dt_themes');?></h3>
                </div>
                <!-- .box-content -->
                <div class="box-content">
                	<p class="note"><?php _e("Select a CATEGORY that should receive a new LEFT SIDE widget area:",'dt_themes');?></p>
                    <?php if(is_array(dttheme_option("widgetarea","left-cats"))):
							$cats = array_unique(dttheme_option("widgetarea","left-cats"));
							foreach($cats as $category):
							  echo "<!-- Category Drop Down Container -->";
							  echo "<div class='multidropdown'>";
							  echo  dttheme_categorylist("widgetarea,left-cats",$category,"multidropdown");						  
							  echo "</div><!-- Category Drop Down Container end-->";
							endforeach;
					  else: 	
						  echo "<!-- Category Drop Down Container -->";
						  echo "<div class='multidropdown'>";
						  echo  dttheme_categorylist("widgetarea,left-cats","","multidropdown");						  
						  echo "</div><!-- Category Drop Down Container end-->";
					  endif?>
                </div><!-- .box-content End-->
            </div><!-- .bpanel-box -->

            <!-- .bpanel-box-->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php _e('Add RIGHT SIDE widget areas','dt_themes');?></h3>
                </div>
                <!-- .box-content -->
                <div class="box-content">
                    <p class="note"><?php _e("Select a CATEGORY that should receive a new RIGHT SIDE widget area:",'dt_themes');?></p>
                    <?php if(is_array(dttheme_option("widgetarea","right-cats"))):
                            $cats = array_unique(dttheme_option("widgetarea","right-cats"));
                            foreach($cats as $category):
                              echo "<!-- Category Drop Down Container -->";
                              echo "<div class='multidropdown'>";
                              echo  dttheme_categorylist("widgetarea,right-cats",$category,"multidropdown");                       
                              echo "</div><!-- Category Drop Down Container end-->";
                            endforeach;
                      else:     
                          echo "<!-- Category Drop Down Container -->";
                          echo "<div class='multidropdown'>";
                          echo  dttheme_categorylist("widgetarea,right-cats","","multidropdown");                          
                          echo "</div><!-- Category Drop Down Container end-->";
                      endif?>
                </div><!-- .box-content End-->
            </div><!-- .bpanel-box -->

        </div><!-- #for-categories end-->
        
        <div id="for-dt_properties" class="tab-content">
        
           	<!-- LEFT .bpanel-box-->
        	<div class="bpanel-box">
            	<div class="box-title">
	                <h3><?php _e('Add LEFT SIDE widget areas for individual Property','dt_themes');?></h3>
                </div>
                
                <!-- .box-content -->
                <div class="box-content">
                	<p class="note"><?php _e("Select a Properties that should receive a new LEFT SIDE widget area:",'dt_themes');?></p><?php
						if(is_array(dttheme_option("widgetarea","left-dt_properties"))):
						
							$posts = array_unique(dttheme_option("widgetarea","left-dt_properties"));
							foreach($posts as $post):
								echo "<!-- Category Drop Down Container -->";
								echo "<div class='multidropdown'>";
								echo  dttheme_custompostlist("dt_properties","widgetarea,left-dt_properties",$post,"multidropdown");
								echo "</div><!-- Category Drop Down Container end-->";
                            endforeach;
                         else:
						 	echo "<!-- Post Drop Down Container -->";
							echo "<div class='multidropdown'>";
							echo  dttheme_custompostlist("dt_properties","widgetarea,left-dt_properties","","multidropdown");
							echo "</div><!-- Category Drop Down Container end-->";						  
                         endif;?>
                </div><!-- .box-content -->
             </div><!-- LEFT .bpanel-box-->

           	<!-- LEFT .bpanel-box-->
        	<div class="bpanel-box">
            	<div class="box-title">
	                <h3><?php _e('Add LEFT SIDE widget areas for individual Property','dt_themes');?></h3>
                </div>
                
                <!-- .box-content -->
                <div class="box-content">
                	<p class="note"><?php _e("Select a Properties that should receive a new RIGHT SIDE widget area:",'dt_themes');?></p>
                    <?php if(is_array(dttheme_option("widgetarea","right-dt_properties"))):
                    	      $posts = array_unique(dttheme_option("widgetarea","right-dt_properties"));
                                foreach($posts as $post):
                                  echo "<!-- Category Drop Down Container -->";
                                  echo "<div class='multidropdown'>";
                                  echo  dttheme_custompostlist("dt_properties","widgetarea,right-dt_properties",$post,"multidropdown");						  
                                  echo "</div><!-- Category Drop Down Container end-->";
                                endforeach;
                           else:
                                 echo "<!-- Post Drop Down Container -->";
                                 echo "<div class='multidropdown'>";
                                 echo  dttheme_custompostlist("dt_properties","widgetarea,right-dt_properties","","multidropdown");						  
                                 echo "</div><!-- Category Drop Down Container end-->";						  
                           endif;?>
                </div><!-- .box-content -->
             </div><!-- LEFT .bpanel-box-->
             
        </div><!-- # for-dt_properties -->


        <div id="for-dt_agents" class="tab-content">
        
           	<!-- LEFT .bpanel-box-->
        	<div class="bpanel-box">
            	<div class="box-title">
	                <h3><?php _e('Add LEFT SIDE widget areas for individual Agent','dt_themes');?></h3>
                </div>
                
                <!-- .box-content -->
                <div class="box-content">
                	<p class="note"><?php _e("Select a Agent that should receive a new LEFT SIDE widget area:",'dt_themes');?></p><?php
					if(is_array(dttheme_option("widgetarea","left-dt_agents"))):
						$posts = array_unique(dttheme_option("widgetarea","left-dt_agents"));
						foreach($posts as $post):
							echo "<!-- Category Drop Down Container -->";
							echo "<div class='multidropdown'>";
							echo  dttheme_custompostlist("dt_agents","widgetarea,left-dt_agents",$post,"multidropdown");
                            echo "</div><!-- Category Drop Down Container end-->";
                         endforeach;
                      else:
                           echo "<!-- Post Drop Down Container -->";
                           echo "<div class='multidropdown'>";
                           echo  dttheme_custompostlist("dt_agents","widgetarea,left-dt_agents","","multidropdown");						  
	                       echo "</div><!-- Category Drop Down Container end-->";						  
                      endif;?>
                </div><!-- .box-content -->
             </div><!-- LEFT .bpanel-box-->

           	<!-- LEFT .bpanel-box-->
        	<div class="bpanel-box">
            	<div class="box-title">
	                <h3><?php _e('Add LEFT SIDE widget areas for individual Agent','dt_themes');?></h3>
                </div>
                
                <!-- .box-content -->
                <div class="box-content">
                	<p class="note"><?php _e("Select a Agent that should receive a new RIGHT SIDE widget area:",'dt_themes');?></p>
                    <?php if(is_array(dttheme_option("widgetarea","right-dt_agents"))):
                    	      $posts = array_unique(dttheme_option("widgetarea","right-dt_agents"));
                                foreach($posts as $post):
                                  echo "<!-- Category Drop Down Container -->";
                                  echo "<div class='multidropdown'>";
                                  echo  dttheme_custompostlist("dt_agents","widgetarea,right-dt_agents",$post,"multidropdown");						  
                                  echo "</div><!-- Category Drop Down Container end-->";
                                endforeach;
                           else:
                                 echo "<!-- Post Drop Down Container -->";
                                 echo "<div class='multidropdown'>";
                                 echo  dttheme_custompostlist("dt_agents","widgetarea,right-dt_agents","","multidropdown");						  
                                 echo "</div><!-- Category Drop Down Container end-->";						  
                           endif;?>
                </div><!-- .box-content -->
             </div><!-- LEFT .bpanel-box-->
             
        </div><!-- # for-dt_agents -->


        <div id="for-dt_agencies" class="tab-content">
        
           	<!-- LEFT .bpanel-box-->
        	<div class="bpanel-box">
            	<div class="box-title">
	                <h3><?php _e('Add LEFT SIDE widget areas for individual Agency','dt_themes');?></h3>
                </div>
                
                <!-- .box-content -->
                <div class="box-content">
                	<p class="note"><?php _e("Select a Agency that should receive a new LEFT SIDE widget area:",'dt_themes');?></p><?php 
						if(is_array(dttheme_option("widgetarea","left-dt_agencies"))):
							$posts = array_unique(dttheme_option("widgetarea","left-dt_agencies"));
							foreach($posts as $post):
								echo "<!-- Category Drop Down Container -->";
								echo "<div class='multidropdown'>";
								echo  dttheme_custompostlist("dt_agencies","widgetarea,left-dt_agencies",$post,"multidropdown");
								echo "</div><!-- Category Drop Down Container end-->";
                                endforeach;
                         else:
						 		echo "<!-- Post Drop Down Container -->";
                                echo "<div class='multidropdown'>";
                                echo  dttheme_custompostlist("dt_agencies","widgetarea,left-dt_agencies","","multidropdown");						  
                                echo "</div><!-- Category Drop Down Container end-->";						  
                          endif;?>
                </div><!-- .box-content -->
             </div><!-- LEFT .bpanel-box-->

           	<!-- LEFT .bpanel-box-->
        	<div class="bpanel-box">
            	<div class="box-title">
	                <h3><?php _e('Add LEFT SIDE widget areas for individual Agency','dt_themes');?></h3>
                </div>
                
                <!-- .box-content -->
                <div class="box-content">
                	<p class="note"><?php _e("Select a Agency that should receive a new RIGHT SIDE widget area:",'dt_themes');?></p>
                    <?php if(is_array(dttheme_option("widgetarea","right-dt_agencies"))):
                    	      $posts = array_unique(dttheme_option("widgetarea","right-dt_agencies"));
                                foreach($posts as $post):
                                  echo "<!-- Category Drop Down Container -->";
                                  echo "<div class='multidropdown'>";
                                  echo  dttheme_custompostlist("dt_agencies","widgetarea,right-dt_agencies",$post,"multidropdown");						  
                                  echo "</div><!-- Category Drop Down Container end-->";
                                endforeach;
                           else:
                                 echo "<!-- Post Drop Down Container -->";
                                 echo "<div class='multidropdown'>";
                                 echo  dttheme_custompostlist("dt_agencies","widgetarea,right-dt_agencies","","multidropdown");						  
                                 echo "</div><!-- Category Drop Down Container end-->";						  
                           endif;?>
                </div><!-- .box-content -->
             </div><!-- LEFT .bpanel-box-->
             
        </div><!-- # for-dt_agencies -->
        
   </div><!-- .bpanel-main-content end-->
</div><!-- #widgetarea end -->