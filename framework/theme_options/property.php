<!-- #property -->
<div id="property" class="bpanel-content">
	<!-- .bpanel-main-content starts here-->
	<div class="bpanel-main-content">

        <ul class="sub-panel">
            <li><a href="#my-property-general"><?php _e("General",'dt_themes');?></a></li>
            <li><a href="#my-property-search"><?php _e("Search",'dt_themes');?></a></li>
            <li><a href="#my-property-archive"><?php _e("Archives",'dt_themes');?></a></li>
            <li><a href="#my-property-membership"><?php _e('Membership','dt_themes'); ?></a></li>
        </ul>

        <!-- General -->
        <div id="my-property-general" class="tab-content">

        	<div class="bpanel-box">
        		<div class="box-title"><h3><?php _e('General','dt_themes');?></h3></div>
        		<div class="box-content">

                    <h6><?php _e('Currency','dt_themes');?></h6>
                    <input type="text" class="small" name="<?php echo "mytheme[property][currency]";?>" value="<?php echo dttheme_option("property","currency");?>">
                    <p class="note no-margin"><?php _e( "Please set default currency",'dt_themes'); ?></p>
                    <div class="clear"></div>
                    <div class="hr"> </div>


                    <h6><?php _e('Area Unit','dt_themes');?></h6>
                    <input type="text" class="large" name="<?php echo "mytheme[property][area_unit]";?>" value="<?php echo dttheme_option("property","area_unit");?>">
                    <p class="note no-margin"><?php _e( "Please set default unit for property area",'dt_themes'); ?></p>
                    <div class="clear"></div>
                    <div class="hr"> </div>


                    <h6><?php _e('Map default latitude','dt_themes');?></h6>
                    <input type="text" class="large" name="<?php echo "mytheme[property][latitude]";?>" value="<?php echo dttheme_option("property","latitude");?>">
                    <p class="note no-margin"><?php _e( "Please set default latitude for map",'dt_themes'); ?></p>
                    <div class="clear"></div>
                    <div class="hr"> </div>

                    <h6><?php _e('Map default longitude','dt_themes');?></h6>
                    <input type="text" class="large" name="<?php echo "mytheme[property][longitude]";?>" value="<?php echo dttheme_option("property","longitude");?>">
                    <p class="note no-margin"><?php _e( "Please set default longitude for map",'dt_themes'); ?></p>
                    <div class="clear"></div>
                    <div class="hr"> </div>

                    <h6><?php _e('Map default Zoom','dt_themes');?></h6>
                    <input type="text" class="small" name="<?php echo "mytheme[property][zoom]";?>" value="<?php echo dttheme_option("property","zoom");?>">
                    <p class="note no-margin"><?php _e( "Please set default zoom level for map",'dt_themes'); ?></p>
                    <div class="clear"></div>
                    <div class="hr"> </div>
        		</div>
        	</div>

            <!-- Socialshare Module -->
            <!-- .bpanel-box-->
            <div class="bpanel-box">
            
                <div class="box-title">
                    <h3><?php _e("Social Shares",'dt_themes'); ?></h3>
                </div>
                
                <div class="box-content">
                    <p class="note no-margin"><?php _e("Manage social share options and its layout to show in the property.",'dt_themes')?></p>
                 
                    <div class="hr_invisible"> </div><?php 

                        global $dttheme_social_bookmarks;
                        $count = 1;

                        foreach($dttheme_social_bookmarks as $social_bookmark):?>

                            <div class="one-half-content <?php echo ($count%2 == 0)?"last":''; ?>">
                            <div class="bpanel-option-set">                        
                                <label><?php echo $social_bookmark["label"];?></label><?php 
                                    $switchclass = (dttheme_option('integration',"property-".$social_bookmark['id'])<>'') ? 'checkbox-switch-on' :'checkbox-switch-off';?>
                                    <div data-for="<?php echo "property-".$social_bookmark['id'];?>" class="checkbox-switch <?php echo $switchclass;?>"></div>
                                    <input id="<?php echo "property-".$social_bookmark['id'];?>" type="checkbox" name="mytheme[integration][<?php echo "property-".$social_bookmark['id'];?>]" 
                                        value="<?php echo $social_bookmark['id'];?>" <?php checked($social_bookmark['id'],dttheme_option('integration',"property-".$social_bookmark['id']));?> class="hidden"/>

                                <div class="hr_invisible"></div><?php 
                                    if(array_key_exists("username",$social_bookmark)):?>
                                        <div class="clear"></div>
                                        <?php _e("Username",'dt_themes');?>
                                            <div class="clear"></div>
                                            <input type="text" class="medium" name="mytheme[integration][<?php echo "property-".$social_bookmark['id']."-username";?>]"
                                                    value="<?php echo dttheme_option('integration',"property-".$social_bookmark['id']."-username");?>" />
                                            <br/><br/><?php
                                    endif;?>

                                <?php if( array_key_exists("options",$social_bookmark)):?>
                                        <div class="clear"></div>
                                        <?php _e("Layout",'dt_themes');?>
                                        <select name="mytheme[integration][<?php echo "property-".$social_bookmark['id']."-layout";?>]"><?php 
                                            foreach($social_bookmark['options'] as $key => $value):
                                                $rs = selected($key,dttheme_option('integration',"property-".$social_bookmark['id']."-layout"),false); ?>
                                                <option value="<?php echo $key?>" <?php echo $rs;?>><?php echo $value?></option><?php 
                                            endforeach;?></select>
                                <?php endif;?>

                                <?php if(array_key_exists("color-scheme",$social_bookmark)): ?>
                                    <div class="hr_invisible"></div><br/>
                                    <?php _e("Color Scheme",'dt_themes');?>
                                    <select name="mytheme[integration][<?php echo "property-".$social_bookmark['id']."-color-scheme";?>]"><?php
                                        foreach($social_bookmark['color-scheme'] as $options):
                                            $rs = selected($options,dttheme_option('integration',"property-".$social_bookmark['id']."-color-scheme"),false);?>
                                            <option value="<?php echo $options?>" <?php echo $rs;?>><?php echo $options?></option><?php 
                                        endforeach;?>
                                    </select>
                                <?php endif;?>

                                <?php if(array_key_exists('lang',$social_bookmark)):?>
                                        <div class="hr_invisible"></div><br/>
                                        <?php _e("Language",'dt_themes');?>
                                        <select name="mytheme[integration][<?php echo "property-".$social_bookmark['id']."-lang";?>]"><?php 
                                            foreach($social_bookmark['lang'] as $key => $value): 
                                                $rs = selected($key,dttheme_option('integration',"property-".$social_bookmark['id']."-lang"),false);?>
                                                <option value="<?php echo $key?>" <?php echo $rs;?>><?php echo $value?></option><?php 
                                            endforeach;?>
                                        </select><?php 
                                    endif;?>

                                <?php if(array_key_exists("text",$social_bookmark)):?>
                                        <div class="clear"></div>
                                        <?php _e("Default Text",'dt_themes');?>
                                        <div class="clear"></div>
                                        <input type="text" class="medium" name="mytheme[integration][<?php echo "property-".$social_bookmark['id']."-text";?>]" value="<?php echo dttheme_option('integration',"property-".$social_bookmark['id']."-text");?>" />
                                        <br/><br/>
                                <?php endif;?>

                                <div class="hr"></div>
                            </div><!-- bpanel-option-set-->
                            </div><!-- .one-half-content--><?php 
                            $count++;
                        endforeach;?>
                </div><!--.box-content end-->
            </div><!-- .bpanel-box end -->    
            <!-- Socialshare Module -->

            <!-- Social Bookmark module -->
            <!-- .bpanel-box-->
            <div class="bpanel-box">
                <div class="box-title">
                    <h3><?php _e("Social Bookmark",'dt_themes'); ?></h3>
                </div>
                <div class="box-content">
                    <p class="note no-margin"><?php _e("Manage social media bookmark options and its layout to show in the property",'dt_themes')?></p>
                    <?php global $dttheme_social_bookmarks;
                      $count = 1;
                        foreach($dttheme_social_bookmarks as $social_bookmark):?>
                        <div class="one-half-content <?php echo ($count%2 == 0)?"last":''; ?>">
                            <div class="bpanel-option-set">
                             <label><?php echo $social_bookmark["label"];?></label>
                                <?php $switchclass = (dttheme_option('integration',"sb-property-".$social_bookmark['id'])<>'') ? 'checkbox-switch-on' :'checkbox-switch-off';?>
                                <div data-for="<?php echo "sb-property-".$social_bookmark['id'];?>" class="checkbox-switch <?php echo $switchclass;?>"></div>
                                <input id="<?php echo "sb-property-".$social_bookmark['id'];?>" type="checkbox" 
                                    name="mytheme[integration][<?php echo "sb-property-".$social_bookmark['id'];?>]" value="<?php echo $social_bookmark['id'];?>" 
                                    <?php checked($social_bookmark['id'],dttheme_option('integration',"sb-property-".$social_bookmark['id']));?>
                                class="hidden"/>
                            </div>
                        </div>
                <?php  $count++;
                         endforeach;?>  
                </div>
            </div><!-- Social Bookmark module end-->
        </div><!-- General -->

        <!-- Search -->
        <div id="my-property-search" class="tab-content">
        	<div class="bpanel-box"><?php
			
				$contract_types = array( "default" =>"Default");
				$terms = get_terms( "contract_type");
				
				if( dttheme_is_plugin_active('sitepress-multilingual-cms/sitepress.php') ){
					
					global $sitepress;
					
					foreach ($terms as $term ) {
						
						$id = icl_object_id( $term->term_id,'contract_type', true, $sitepress->get_default_language() );
						if( $term->term_id == $id ) {
							$contract_types[$term->term_id] = $term->name;
						}
					}

				} else {
					
					foreach ($terms as $term ) {
						$contract_types[$term->term_id] = $term->name;
					}
				}
				
				foreach( $contract_types as $key => $value ): ?>
    	    				<!-- Section 2 -->
    	    				<div class="box-title"><h3><?php echo $value.__(' Tab Settings','dt_themes');?></h3></div>
    	    				<div class="box-content">

    	    					<h6><?php _e('Enable Search Based on ','dt_themes'); echo $value; _e( ' Contract type','dt_themes'); ?></h6>
    	    					<?php dttheme_switch("",'property',"enable-{$key}-search");?>

    	    					<div class="clear"></div>
    	    					<div class="hr"> </div>

    	    					<h6><?php _e('Title','dt_themes');?></h6>
                                <?php if( dttheme_is_plugin_active('sitepress-multilingual-cms/sitepress.php') ){
											global $sitepress;
											$languages = $sitepress->get_active_languages();
											
											foreach( $languages as $lang_key => $array ) { ?>
                                            	<input type="text" class="large" name="<?php echo "mytheme[property][{$lang_key}-{$key}-title]";?>"
                                                	 value="<?php echo dttheme_option("property","{$lang_key}-{$key}-title");?>">
                                                <p class="note no-margin"><?php _e( "please give title for the tab language : ",'dt_themes'); echo $array['native_name'];?></p><?php
											}
											

								} else { ?>
	    	    					<input type="text" class="large" name="<?php echo "mytheme[property][{$key}-title]";?>" 
                                    	   value="<?php echo dttheme_option("property","{$key}-title");?>">
                                           <p class="note no-margin"><?php _e( "please give title for the tab",'dt_themes'); ?></p><?php
								}?>
    	    					<div class="clear"></div>
    	    					<div class="hr"> </div>

    	    					<h6><?php _e('Available Search Modules','dt_themes');?></h6>

    	    					<div class="column one-third">
    	    						<div class="column one-fifth">
    	    							<?php dttheme_switch("",'property',"enable-title-module-for-{$key}");?>
    	    						</div>
    	    						<div class="column four-fifth last">
    	    							<p class="note no-margin"><?php _e( "Enable Title Module",'dt_themes'); ?></p>
    	    						</div>
    	    					</div>

    	    					<div class="column one-third">
    	    						<div class="column one-fifth">
    	    							<?php dttheme_switch("",'property',"enable-property-type-module-for-{$key}");?>
    	    						</div>
    	    						<div class="column four-fifth last">
    	    							<p class="note no-margin"><?php _e( "Enable Property Type Module",'dt_themes'); ?></p>
    	    						</div>
    	    					</div>

    	    					<div class="column one-third last">
    	    						<div class="column one-fifth">
    	    							<?php dttheme_switch("",'property',"enable-property-location-module-for-{$key}");?>
    	    						</div>
    	    						<div class="column four-fifth last">
    	    							<p class="note no-margin"><?php _e( "Enable Property Location Module",'dt_themes'); ?></p>
    	    						</div>
    	    					</div>

    	    					<div class="hr"> </div>

    	    					<div class="column one-half">
                                
                                	<div class="clear"></div>
                                    <h6><?php _e('Beds','dt_themes');?></h6>

    	    						<div class="column one-fifth">
    	    							<?php dttheme_switch("",'property',"enable-property-beds-meta-for-{$key}");?>
    	    						</div>
    	    						<div class="column four-fifth last">
    	    							<p class="note no-margin"><?php _e( "Enable Property Beds Meta",'dt_themes'); ?></p>
    	    						</div>

                                    <div class="clear"></div>
                                    <!-- Range -->
    	    						<div class="property-price">
    	    							<a href='' class='add-price-item'><?php _e('Add Range','dt_themes');?></a>
    	    							<?php $beds = dttheme_option("property","beds-for-{$key}");
    	    								  $beds = is_array($beds) ? array_filter($beds) : array();
    	    								  $beds = array_unique( $beds);
    	    								foreach( $beds as $bed ){ ?>
    	    								<div class="property-price-container">
    	    									<input type="text" class="medium" name="<?php echo "mytheme[property][beds-for-{$key}][]";?>" value="<?php echo $bed;?>">
    	    									<a href='' class='remove-price-item'><?php _e('Remove','dt_themes');?></a>
    	    								</div>
    	    							<?php } ?>
    	    							<div class="clone hidden">
    	    								<div class="property-price-container">
    	    									<input type="text" class="medium" name="<?php echo "mytheme[property][beds-for-{$key}][]";?>" value="">
    	    									<a href='' class='remove-price-item'><?php _e('Remove','dt_themes');?></a>
    	    								</div>
    	    							</div>	
    	    						</div><!-- Range -->
                                    
    	    					</div>

    	    					<div class="column one-half last">
                                
                                	<div class="clear"></div>
                                    <h6><?php _e('Baths','dt_themes');?></h6>
                                    
    	    						<div class="column one-fifth">
    	    							<?php dttheme_switch("",'property',"enable-property-baths-meta-for-{$key}");?>
    	    						</div>
    	    						<div class="column four-fifth last">
    	    							<p class="note no-margin"><?php _e( "Enable Property Baths Meta",'dt_themes'); ?></p>
    	    						</div>

                                    <div class="clear"></div>
                                    <!-- Range -->
    	    						<div class="property-price">
    	    							<a href='' class='add-price-item'><?php _e('Add Range','dt_themes');?></a>
    	    							<?php $baths = dttheme_option("property","baths-for-{$key}");
    	    								  $baths = is_array($baths) ? array_filter($baths) : array();
    	    								  $baths = array_unique( $baths);
    	    								foreach( $baths as $bath ){ ?>
    	    								<div class="property-price-container">
    	    									<input type="text" class="medium" name="<?php echo "mytheme[property][baths-for-{$key}][]";?>" value="<?php echo $bath;?>">
    	    									<a href='' class='remove-price-item'><?php _e('Remove','dt_themes');?></a>
    	    								</div>
    	    							<?php } ?>
    	    							<div class="clone hidden">
    	    								<div class="property-price-container">
    	    									<input type="text" class="medium" name="<?php echo "mytheme[property][baths-for-{$key}][]";?>" value="">
    	    									<a href='' class='remove-price-item'><?php _e('Remove','dt_themes');?></a>
    	    								</div>
    	    							</div>	
    	    						</div><!-- Range -->
                                    
    	    					</div>

    	    					<div class="column one-half">
                                
                                	<div class="clear"></div>
                                	<h6><?php _e('Floors','dt_themes');?></h6>
                                    
    	    						<div class="column one-fifth">
    	    							<?php dttheme_switch("",'property',"enable-property-floors-meta-for-{$key}");?>
    	    						</div>
    	    						<div class="column four-fifth last">
    	    							<p class="note no-margin"><?php _e( "Enable Property Floors Meta",'dt_themes'); ?></p>
    	    						</div>

                                    <div class="clear"></div>
                                    <!-- Range -->
    	    						<div class="property-price">
    	    							<a href='' class='add-price-item'><?php _e('Add Range','dt_themes');?></a>
    	    							<?php $floors = dttheme_option("property","floors-for-{$key}");
    	    								  $floors = is_array($floors) ? array_filter($floors) : array();
    	    								  $floors = array_unique( $floors);
    	    								foreach( $floors as $floor ){ ?>
    	    								<div class="property-price-container">
    	    									<input type="text" class="medium" name="<?php echo "mytheme[property][floors-for-{$key}][]";?>" value="<?php echo $floor;?>">
    	    									<a href='' class='remove-price-item'><?php _e('Remove','dt_themes');?></a>
    	    								</div>
    	    							<?php } ?>
    	    							<div class="clone hidden">
    	    								<div class="property-price-container">
    	    									<input type="text" class="medium" name="<?php echo "mytheme[property][floors-for-{$key}][]";?>" value="">
    	    									<a href='' class='remove-price-item'><?php _e('Remove','dt_themes');?></a>
    	    								</div>
    	    							</div>	
    	    						</div><!-- Range -->
                                    
    	    					</div>

    	    					<div class="column one-half last">
                                
	                                <div class="clear"></div>
                                	<h6><?php _e('Garages','dt_themes');?></h6>
                                    
    	    						<div class="column one-fifth">
    	    							<?php dttheme_switch("",'property',"enable-property-garages-meta-for-{$key}");?>
    	    						</div>
    	    						<div class="column four-fifth last">
    	    							<p class="note no-margin"><?php _e( "Enable Property Garages Meta",'dt_themes'); ?></p>
    	    						</div>
                                    
                                    <div class="clear"></div>
                                    <!-- Range -->
    	    						<div class="property-price">
    	    							<a href='' class='add-price-item'><?php _e('Add Range','dt_themes');?></a>
    	    							<?php $garages = dttheme_option("property","garages-for-{$key}");
    	    								  $garages = is_array($garages) ? array_filter($garages) : array();
    	    								  $garages = array_unique( $garages);
    	    								foreach( $garages as $garage ){ ?>
    	    								<div class="property-price-container">
    	    									<input type="text" class="medium" name="<?php echo "mytheme[property][garages-for-{$key}][]";?>" value="<?php echo $garage;?>">
    	    									<a href='' class='remove-price-item'><?php _e('Remove','dt_themes');?></a>
    	    								</div>
    	    							<?php } ?>
    	    							<div class="clone hidden">
    	    								<div class="property-price-container">
    	    									<input type="text" class="medium" name="<?php echo "mytheme[property][garages-for-{$key}][]";?>" value="">
    	    									<a href='' class='remove-price-item'><?php _e('Remove','dt_themes');?></a>
    	    								</div>
    	    							</div>	
    	    						</div><!-- Range -->
                                    
                                    
    	    					</div>

    	    					<div class="hr"> </div>
                                <h6><?php _e('Price Modules','dt_themes');?></h6>

    	    					<div class="column one-half">
    	    						<div class="column one-fifth">
    	    							<?php dttheme_switch("",'property',"enable-property-min-price-for-{$key}");?>
    	    						</div>
    	    						<div class="column four-fifth last">
    	    							<p class="note no-margin"><?php _e( "Enable Minimum Price Search",'dt_themes'); ?></p>
    	    						</div>
    	    						<div class="clear"></div>

    	    						<div class="property-price">
    	    							<a href='' class='add-price-item'><?php _e('Add Range','dt_themes');?></a>
    	    							<?php $prices = dttheme_option("property","min-price-for-{$key}");
    	    								  $prices = is_array($prices) ? array_filter($prices) : array();
    	    								  $prices = array_unique( $prices);
    	    								foreach( $prices as $price ){ ?>
    	    								<div class="property-price-container">
    	    									<input type="text" class="medium" name="<?php echo "mytheme[property][min-price-for-{$key}][]";?>" value="<?php echo $price;?>">
    	    									<a href='' class='remove-price-item'><?php _e('Remove','dt_themes');?></a>
    	    								</div>
    	    							<?php } ?>
    	    							<div class="clone hidden">
    	    								<div class="property-price-container">
    	    									<input type="text" class="medium" name="<?php echo "mytheme[property][min-price-for-{$key}][]";?>" value="">
    	    									<a href='' class='remove-price-item'><?php _e('Remove','dt_themes');?></a>
    	    								</div>
    	    							</div>	
    	    						</div>
    	    					</div>

    	    					<div class="column one-half last">
    	    						<div class="column one-fifth">
    	    							<?php dttheme_switch("",'property',"enable-property-max-price-for-{$key}");?>
    	    						</div>
    	    						<div class="column four-fifth last">
    	    							<p class="note no-margin"><?php _e( "Enable Maximum Price Search",'dt_themes'); ?></p>
    	    						</div>
    	    						<div class="clear"></div>
    	    						<div class="property-price">
    	    							<a href='' class='add-price-item'><?php _e('Add Range','dt_themes');?></a>
    	    							<?php $prices = dttheme_option("property","max-price-for-{$key}");
    	    								  $prices = is_array($prices) ? array_filter($prices) : array();
    	    								  $prices = array_unique( $prices);
    	    								foreach( $prices as $price ){ ?>
    	    								<div class="property-price-container">
    	    									<input type="text" class="medium" name="<?php echo "mytheme[property][max-price-for-{$key}][]";?>" value="<?php echo $price;?>">
    	    									<a href='' class='remove-price-item'><?php _e('Remove','dt_themes');?></a>
    	    								</div>
    	    							<?php } ?>
    	    							<div class="clone hidden">
    	    								<div class="property-price-container">
    	    									<input type="text" class="medium" name="<?php echo "mytheme[property][max-price-for-{$key}][]";?>" value="">
    	    									<a href='' class='remove-price-item'><?php _e('Remove','dt_themes');?></a>
    	    								</div>
    	    							</div>	
    	    						</div>
    	    					</div>
    	    				</div><!-- Section 2 -->
	        	<?php 	endforeach; ?>
        	</div>
        </div><!-- Search -->

        <!-- Archives -->
        <div id="my-property-archive" class="tab-content">
            <div class="bpanel-box">

                <!-- Property  Archive -->
                <div class="box-title"><h3><?php _e('Properties Archive layout','dt_themes');?></h3></div>
                <div class="box-content">   
                    <p class="note"><?php _e('Archive page for properties ( properties listing page) will use the layout you select below','dt_themes');?></p>
                    <div class="bpanel-option-set">
                        <ul class="bpanel-layout-set"><?php
                            $posts_layout = array('content-full-width'=>'without-sidebar','with-left-sidebar'=>'left-sidebar','with-right-sidebar'=>'right-sidebar');

                            $v = dttheme_option('property',"property-archive-page-layout");
                            $v = !empty($v) ? $v : "with-left-sidebar";

                            foreach($posts_layout as $key => $value):
                                $class = ( $key ==  $v ) ? " class='selected' " :"";                                  
                                echo "<li><a href='#' rel='{$key}' {$class} title='{$value}'><img src='".IAMD_FW_URL."theme_options/images/columns/{$value}.png' /></a></li>";
                            endforeach;?></ul>
                            <input name="mytheme[property][property-archive-page-layout]" type="hidden" value="<?php echo dttheme_option('property',"property-archive-page-layout");?>"/>
                    </div>
                </div>

                <div class="box-title"><h3><?php _e('Properties Archive post layout ','dt_themes');?></h3></div>
                <div class="box-content">
                    <p class="note"><?php _e('Properties posts will use the layout you select below','dt_themes');?></p>
                    <div class="bpanel-option-set">
                        <ul class="bpanel-layout-set"><?php
                            $posts_layout = array('one-column'=>__("Single post per row.",'dt_themes')
                                ,'one-half-column'=>__("Two posts per row.",'dt_themes')
                                ,'one-third-column'=>__("Three posts per row.",'dt_themes')
                                ,'one-fourth-column'=>__("Four posts per row.",'dt_themes')
                                ,'list-view' => __('List View','dt_themes'));

                            $v = dttheme_option('property',"property-archive-post-layout");
                            $v = !empty($v) ? $v : "one-half-column";

                            foreach($posts_layout as $key => $value):
                                $class = ( $key ==  $v ) ? " class='selected' " :"";                                  
                                echo "<li><a href='#' rel='{$key}' {$class} title='{$value}'><img src='".IAMD_FW_URL."theme_options/images/columns/{$key}.png' /></a></li>";
                            endforeach;?></ul>
                            <input name="mytheme[property][property-archive-post-layout]" type="hidden" value="<?php echo dttheme_option('property',"property-archive-post-layout");?>"/>
                    </div>
                </div>                
                <!-- Property  Archive -->

                <!-- Agent  Archive -->
                <div class="box-title"><h3><?php _e('Agents Archive layout','dt_themes');?></h3></div>
                <div class="box-content">   
                    <p class="note"><?php _e('Archive page for agents ( Agent listing page) will use the layout you select below','dt_themes');?></p>
                    <div class="bpanel-option-set">
                        <ul class="bpanel-layout-set"><?php
                            $posts_layout = array('content-full-width'=>'without-sidebar','with-left-sidebar'=>'left-sidebar','with-right-sidebar'=>'right-sidebar');

                            $v = dttheme_option('property',"agent-archive-page-layout");
                            $v = !empty($v) ? $v : "with-left-sidebar";

                            foreach($posts_layout as $key => $value):
                                $class = ( $key ==  $v ) ? " class='selected' " :"";                                  
                                echo "<li><a href='#' rel='{$key}' {$class} title='{$value}'><img src='".IAMD_FW_URL."theme_options/images/columns/{$value}.png' /></a></li>";
                            endforeach;?></ul>
                            <input name="mytheme[property][agent-archive-page-layout]" type="hidden" value="<?php echo dttheme_option('property',"agent-archive-page-layout");?>"/>
                    </div>
                </div>

                <div class="box-title"><h3><?php _e('Agents Archive post layout ','dt_themes');?></h3></div>
                <div class="box-content">
                    <p class="note"><?php _e('Agents posts will use the layout you select below','dt_themes');?></p>
                    <div class="bpanel-option-set">
                        <ul class="bpanel-layout-set"><?php
                            $posts_layout = array('one-column'=>__("Single post per row.",'dt_themes')
                                ,'one-half-column'=>__("Two posts per row.",'dt_themes')
                                ,'one-third-column'=>__("Three posts per row.",'dt_themes')
                                ,'one-fourth-column'=>__("Four posts per row.",'dt_themes'));

                            $v = dttheme_option('property',"agent-archive-post-layout");
                            $v = !empty($v) ? $v : "one-half-column";

                            foreach($posts_layout as $key => $value):
                                $class = ( $key ==  $v ) ? " class='selected' " :"";                                  
                                echo "<li><a href='#' rel='{$key}' {$class} title='{$value}'><img src='".IAMD_FW_URL."theme_options/images/columns/{$key}.png' /></a></li>";
                            endforeach;?></ul>
                            <input name="mytheme[property][agent-archive-post-layout]" type="hidden" value="<?php echo dttheme_option('property',"agent-archive-post-layout");?>"/>
                    </div>
                </div>                
                <!-- Agent  Archive -->

                <!-- Agency  Archive -->
                <div class="box-title"><h3><?php _e('Agencies Archive layout','dt_themes');?></h3></div>
                <div class="box-content">   
                    <p class="note"><?php _e('Archive page for agencies ( Agencies listing page) will use the layout you select below','dt_themes');?></p>
                    <div class="bpanel-option-set">
                        <ul class="bpanel-layout-set"><?php
                            $posts_layout = array('content-full-width'=>'without-sidebar','with-left-sidebar'=>'left-sidebar','with-right-sidebar'=>'right-sidebar');

                            $v = dttheme_option('property',"agency-archive-page-layout");
                            $v = !empty($v) ? $v : "with-left-sidebar";

                            foreach($posts_layout as $key => $value):
                                $class = ( $key ==  $v ) ? " class='selected' " :"";                                  
                                echo "<li><a href='#' rel='{$key}' {$class} title='{$value}'><img src='".IAMD_FW_URL."theme_options/images/columns/{$value}.png' /></a></li>";
                            endforeach;?></ul>
                            <input name="mytheme[property][agency-archive-page-layout]" type="hidden" value="<?php echo dttheme_option('property',"agency-archive-page-layout");?>"/>
                    </div>
                </div>

                <div class="box-title"><h3><?php _e('Agencies Archive post layout ','dt_themes');?></h3></div>
                <div class="box-content">
                    <p class="note"><?php _e('Agencies posts will use the layout you select below','dt_themes');?></p>
                    <div class="bpanel-option-set">
                        <ul class="bpanel-layout-set"><?php
                            $posts_layout = array('one-column'=>__("Single post per row.",'dt_themes')
                                ,'one-half-column'=>__("Two posts per row.",'dt_themes')
                                ,'one-third-column'=>__("Three posts per row.",'dt_themes')
                                ,'one-fourth-column'=>__("Four posts per row.",'dt_themes'));

                            $v = dttheme_option('property',"agency-archive-post-layout");
                            $v = !empty($v) ? $v : "one-half-column";

                            foreach($posts_layout as $key => $value):
                                $class = ( $key ==  $v ) ? " class='selected' " :"";                                  
                                echo "<li><a href='#' rel='{$key}' {$class} title='{$value}'><img src='".IAMD_FW_URL."theme_options/images/columns/{$key}.png' /></a></li>";
                            endforeach;?></ul>
                            <input name="mytheme[property][agency-archive-post-layout]" type="hidden" value="<?php echo dttheme_option('property',"agency-archive-post-layout");?>"/>
                    </div>
                </div>
                <!-- Agency  Archive -->

            </div><!-- .bpanel-box -->
        </div><!-- Archives -->

        <!-- Membership -->
        <div id="my-property-membership" class="tab-content">
            <div class="bpanel-box"><?php
                if( dttheme_is_plugin_active('s2member/s2member.php') ):?>
                    <div class="box-title">
                        <h3><?php _e('Submit Limit','dt_themes');?></h3>
                        <?php _e('Properties submission limit for individual membership level','dt_themes');?></div>

                    <div class="box-content"><?php

                        for ($n = 0; $n <= $GLOBALS["WS_PLUGIN__"]["s2member"]["c"]["levels"]; $n++) { 

                            $v = dttheme_option("property","s2-{$n}-allowed-post");
                            $s = dttheme_option("property","s2-{$n}-allowed-period");

                            echo '<div class="membership-level"><p>';
                            echo do_shortcode("[s2Get constant='S2MEMBER_LEVEL{$n}_LABEL'/]");
                            _e("s can submit ","dt_themes");
                            echo "</p><input type='text' class='small' name='mytheme[property][s2-{$n}-allowed-post]' value='{$v}'/>";
                              _e(" items for  ","dt_themes");
                            echo "<select name='mytheme[property][s2-{$n}-allowed-period]'>";
                                    $periods = array("Day","Week","Month","Year");
                                    foreach( $periods as $period ){
                                        $selected = selected( $s, $period, false );
                                        echo "<option value='{$period}' {$selected}>{$period}</option>";
                                    }
                            echo "</select>";
                            echo '</div>';
                            echo '<div class="clear"></div><div class="hr"> </div>';
                        }?>

                        <h6><?php _e('Warning Message','dt_themes');?></h6>
                        <textarea id="mytheme-s2member-msg" name="mytheme[property][s2-member-msg]" style="width:547px;height:155px;"
                            rows="" cols=""><?php echo htmlspecialchars (stripslashes(dttheme_option('property','s2-member-msg')));?></textarea>
                        <p class="note"> <?php _e('You can paste text in this box. This will be shown when member submit request exceeds.','dt_themes');?> </p>


                    </div><?php
                else:?>    
                    <div class="box-title"><h3><?php _e('Warning','dt_themes');?></h3></div>
                    <div class="box-content"><p class="note"><?php _e("You have to install and activate the S2Members plugin to use this module ..",'dt_themes');?></p></div><?php
                endif;?>                    
            </div>
        </div><!-- Membership -->

    </div> <!-- .bpanel-main-content ends here-->
</div><!-- #property -->