<?php
function dt_get_page_permalink_by_its_template( $temlplate ) {
	$permalink = null;

	$pages = get_posts( array(
			'post_type' => 'page',
			'meta_key' => '_wp_page_template',
			'suppress_filters' => false,
			'meta_value' => $temlplate ) );

	if ( is_array( $pages ) && count( $pages ) > 0 ) {
		$login_page = $pages[0];
		$permalink = get_permalink( $login_page->ID );
	}
	return $permalink;
}

function property_title_filter( $where, &$wp_query ) {
	global $wpdb;
	if ( $search_term = $wp_query->get( 'search_proprty_title' ) ) {
		$where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( like_escape( $search_term ) ) . '%\'';
    }
    return $where;
}

function dt_delete_property( $pid ) {	

	wp_delete_post(  $pid );
		
	$current_url = add_query_arg('action','deleted', get_permalink() );
	wp_redirect( $current_url );
}

function dt_list_properties_by_user( $user_id ){
	global $post;
	$pagenum = isset( $_GET['pagenum'] ) ? intval( $_GET['pagenum'] ) : 1;

	$args = array(
		'order' => 'ASC',
		'orderby' => 'date',
		'author' => $user_id,
		'post_status' => array('draft', 'future', 'pending', 'publish'),
		'post_type' => 'dt_properties',
		'posts_per_page' => 10,
		'paged' => $pagenum
	);

	$properties = new WP_Query( $args );

		if( $properties-> have_posts() ) : ?>

			<table class="property-list-table">
				<caption><h2 class="border-title"><span><?php _e('Properties List','dt_themes');?></span></h2>
				<div class="dt-sc-hr-invisible-small"> </div>
				</caption>
				<thead>
					<tr>
						<th><?php _e('ID','dt_themes');?></th>
						<th><?php _e('Title','dt_themes');?></th>
						<th><?php _e('Created','dt_themes');?></th>
						<th><?php _e('Status','dt_themes');?></th>
						<th><?php _e('Action','dt_themes');?></th>
					</tr>
				</thead>
				<tbody>
				<?php while( $properties-> have_posts() ):  $properties->the_post(); ?>

						<tr>
							<td><?php the_ID(); ?></td>
							<td><?php 
								if ( in_array( $post->post_status, array('draft', 'future', 'pending') ) ) {
									the_title();
								} else { ?>
									<a href="<?php the_permalink(); ?>" target="_blank"><?php the_title(); ?></a><?php
								} ?></td>
							<td><?php echo get_the_date( get_option('date_format') ); ?></td>
							<td><?php echo $post->post_status;?></td>
							<td>
								<a class="edit-link" href="?action=edit&pid=<?php echo $post->ID;?>">
									<i class="fa fa-pencil"> </i><?php _e('Edit','dt_themes');?></a> -
								<a class="delete-link" href="?action=delete&pid=<?php echo $post->ID;?>" onclick="return confirm('<?php _e('Are you sure to delete this post?','dt_themes');?> ');">
									<i class="fa fa-trash-o"> </i><?php _e('Delete','dt_themes');?></a>
							</td>
					</tr>

				<?php endwhile;?>
				</tbody>
			</table>
			<div class="property-pagination"><?php
            	$pagination = paginate_links( array(
                    'base' => add_query_arg( 'pagenum', '%#%' ),
                    'format' => '',
                    'prev_text' => __( '&laquo;', 'wpuf' ),
                    'next_text' => __( '&raquo;', 'wpuf' ),
                    'total' => $properties->max_num_pages,
                    'current' => $pagenum
                ) );

                if ( $pagination ) { echo $pagination; } ?></div><?php
		endif;
}

function dt_property_form( $user_id, $action, $post_id = 0 ) {

	if( isset($_POST['dt-propery-submit']) && $action === "add" ) {
		$p_title = wp_strip_all_tags($_POST['dt-property-title']);
		$p_author = $user_id;
		$p_content = trim($_POST['dt-property-content-area']);

		$current_post = array( 
			'post_title' => $p_title,
			'post_author' => $p_author,
			'post_content' => $p_content,
			'post_date' => date_i18n('Y-m-d H:i:s', time()),
			'post_type' => 'dt_properties',
			'post_status' => 'draft');

		$post_id = wp_insert_post( $current_post );		

		if( $_POST['dt-property-contract'] !== '0' ){
			wp_set_object_terms( $post_id , array_map('intval', array($_POST['dt-property-contract']) ),'contract_type' );
		}

		if( $_POST['dt-property-type'] !== '0' ){
			wp_set_object_terms( $post_id , array_map('intval', array($_POST['dt-property-type']) ),'property_type' );
		}

		if( $_POST['dt-property-location'] !== '0' ){
			wp_set_object_terms( $post_id , array_map('intval', array($_POST['dt-property-location']) ),'property_location');
		}

		if(  array_key_exists('dt-property-amenities', $_POST) ){
			$property_amenities = array_filter( $_POST['dt-property-amenities']);
			wp_set_object_terms( $post_id , array_map('intval', $property_amenities), 'property_amenities' );
		}

		#update post meta
		if( $post_id > 0 ) {

			#Address
			if( isset( $_POST['dt-property-address']) && !empty( $_POST['dt-property-address']) ){
				update_post_meta ( $post_id, "_property_address", stripslashes ( $_POST ['dt-property-address'] ) );
			}

			#Price
			if( isset( $_POST['dt-property-price']) && !empty( $_POST['dt-property-price']) ){
				update_post_meta ( $post_id, "_property_price", stripslashes ( $_POST ['dt-property-price'] ) );
			}

			#GPS
			if( isset($_POST['gps'] ) ){
				$gps = array_filter( $_POST['gps']);
				update_post_meta ( $post_id, "_property_gps", $gps );
			}

			#Media
			$media = array();
			$media ['items'] = isset ( $_POST ['items'] ) ? $_POST ['items'] : "";
			$media['items_id'] = isset ( $_POST ['items_id'] ) ? $_POST ['items_id'] : "";
			$media ['items_thumbnail'] = isset ( $_POST ['items_thumbnail'] ) ? $_POST ['items_thumbnail'] : "";
			$media ['items_name'] = isset ( $_POST ['items_name'] ) ? $_POST ['items_name'] : "";

			$media = array_filter( $media);
			update_post_meta ( $post_id, "_property_media",$media);

			#Others
				#Bed Rooms
				if( isset ( $_POST ['dt-property-bedrooms'] ) ){
					update_post_meta ( $post_id, "_bedrooms", stripslashes ( $_POST ['dt-property-bedrooms'] ) );	
				}

				#Bath Rooms
				if( isset ( $_POST ['dt-property-bathrooms'] ) ){
					update_post_meta ( $post_id, "_bathrooms", stripslashes ( $_POST ['dt-property-bathrooms'] ) );	
				}

				#Floors
				if( isset ( $_POST ['dt-property-floors'] ) ){
					update_post_meta ( $post_id, "_floors", stripslashes ( $_POST ['dt-property-floors'] ) );	
				}

				#Parkings
				if( isset ( $_POST ['dt-property-parkings'] ) ){
					update_post_meta ( $post_id, "_parking", stripslashes ( $_POST ['dt-property-parkings'] ) );	
				}

				#Area
				if( isset ( $_POST ['dt-property-area'] ) ){
					update_post_meta ( $post_id, "_area", stripslashes ( $_POST ['dt-property-area'] ) );	
				}

				if( isset ( $_POST ['agency'] ) ){
					update_post_meta ( $post_id, "_property_agency", stripslashes ( $_POST ['agency'] ) );	
				}

				if( isset ( $_POST ['agent'] ) ){
					update_post_meta ( $post_id, "_property_agent", stripslashes ( $_POST ['agent'] ) );	
				}
			#Others End	
		}
		
		$current_url = $_SERVER["REQUEST_URI"];
		$current_url = add_query_arg('action','added',$current_url);
		$post_id = 0;
		wp_redirect( $current_url );
		exit;
	} elseif( isset($_POST['dt-propery-submit']) && $action == "edit" ) {

		$p_id = $_POST['dt-property-id'];
		$status = $_POST['dt-property-status'];
		$p_title = wp_strip_all_tags($_POST['dt-property-title']);
		$p_content = trim($_POST['dt-property-content-area']);
		$p_author = $user_id;

		$current_post = array(
			'ID' => $p_id, 
			'post_title' => $p_title,
			'post_author' => $p_author,
			'post_content' => $p_content,
			#'post_date' => date_i18n('Y-m-d H:i:s', time()),
			'post_type' => 'dt_properties',
			#'post_status' => $status
			'post_status' => 'draft');

		$post_id = wp_update_post( $current_post );
		if( $post_id > 0 ){
			wp_set_object_terms( $post_id , array_map('intval', array($_POST['dt-property-contract']) ),'contract_type' );
			wp_set_object_terms( $post_id , array_map('intval', array($_POST['dt-property-type']) ),'property_type' );
			wp_set_object_terms( $post_id , array_map('intval', array($_POST['dt-property-location']) ),'property_location');
			$property_amenities =  isset( $_POST['dt-property-amenities'] ) ? $_POST['dt-property-amenities'] : array();
			wp_set_object_terms( $post_id , array_map('intval', $property_amenities), 'property_amenities' );
		}

		#post meta
			#Sub Title
			if( isset( $_POST['dt-property-video']) )
				update_post_meta ( $post_id, "_property_featured_video", stripslashes ( $_POST ['dt-property-video'] ) );

			#Address
			if( isset( $_POST['dt-property-address']) )
				update_post_meta ( $post_id, "_property_address", stripslashes ( $_POST ['dt-property-address'] ) );
			
			#Price
			if( isset( $_POST['dt-property-price']) )
				update_post_meta ( $post_id, "_property_price", stripslashes ( $_POST ['dt-property-price'] ) );
					
			#GPS
			if( isset($_POST['gps'] ) ){
				$gps = array_filter( $_POST['gps']);
				update_post_meta ( $post_id, "_property_gps", $gps );
			}

			#Others
				#Bed Rooms
				if( isset ( $_POST ['dt-property-bedrooms'] ) ){
					update_post_meta ( $post_id, "_bedrooms", stripslashes ( $_POST ['dt-property-bedrooms'] ) );	
				}

				#Bath Rooms
				if( isset ( $_POST ['dt-property-bathrooms'] ) ){
					update_post_meta ( $post_id, "_bathrooms", stripslashes ( $_POST ['dt-property-bathrooms'] ) );	
				}

				#Floors
				if( isset ( $_POST ['dt-property-floors'] ) ){
					update_post_meta ( $post_id, "_floors", stripslashes ( $_POST ['dt-property-floors'] ) );	
				}

				#Parkings
				if( isset ( $_POST ['dt-property-parkings'] ) ){
					update_post_meta ( $post_id, "_parking", stripslashes ( $_POST ['dt-property-parkings'] ) );	
				}

				#Area
				if( isset ( $_POST ['dt-property-area'] ) ){
					update_post_meta ( $post_id, "_area", stripslashes ( $_POST ['dt-property-area'] ) );	
				}

				if( isset ( $_POST ['agency'] ) ){
					update_post_meta ( $post_id, "_property_agency", stripslashes ( $_POST ['agency'] ) );	
				}

				if( isset ( $_POST ['agents'] ) ){
					update_post_meta ( $post_id, "_property_agent",  $_POST ['agents']  );	
				}
			#Others End	
		#post meta end

		#Media
		$media = array();
		$media ['items'] = isset ( $_POST ['items'] ) ? $_POST ['items'] : "";
		$media['items_id'] = isset ( $_POST ['items_id'] ) ? $_POST ['items_id'] : "";
		$media ['items_thumbnail'] = isset ( $_POST ['items_thumbnail'] ) ? $_POST ['items_thumbnail'] : "";
		$media ['items_name'] = isset ( $_POST ['items_name'] ) ? $_POST ['items_name'] : "";
		$media = array_filter( $media);
		update_post_meta ( $post_id, "_property_media",$media);

		$current_url = $_SERVER["REQUEST_URI"];
		$current_url = add_query_arg('action','edited',$current_url);
		$current_url = remove_query_arg('pid', $current_url);
		wp_redirect( $current_url );
		
	}

	# action = Add
	$title = $property_video = $content = $address = $location = $contract = $property = $price = $price_prefix = $bedrooms = $bathrooms = $floors = $parking = $area = $post_status = "";
	$latitude = dttheme_option("property","latitude");
	$longitude = dttheme_option("property","longitude");

	$amenities = array();
	$form_title = __('Submit Your Property','dt_themes');
	$submit = __("Submit Property","dt_themes");

	# action = Edit
	if( isset($_REQUEST['action']) && ( $_REQUEST['action'] === "edit") ) {
		$form_title = __('Update Your Property','dt_themes');
		$submit = __("Update Property","dt_themes");

		$current_post = get_post($post_id);
		$content = $current_post->post_content;
		$title = esc_html( $current_post->post_title );
		$post_status = $current_post->post_status;
		$property_video = get_post_meta( $post_id, "_property_featured_video","true");
		$address = get_post_meta( $post_id, "_property_address","true");
		$price = get_post_meta( $post_id,"_property_price",true);
		$bedrooms = get_post_meta ( $post_id, "_bedrooms",true);
		$bathrooms = get_post_meta ( $post_id, "_bathrooms",true);
		$floors = get_post_meta ( $post_id, "_floors",true);
		$parking = get_post_meta ( $post_id, "_parking",true);
		$area = get_post_meta ( $post_id, "_area",true);
		$selected_agency  =  get_post_meta ( $post_id, "_property_agency",true);
		$gps = get_post_meta( $post_id, "_property_gps","true");
		$latitude = is_array($gps) && array_key_exists("latitude", $gps) ? $gps['latitude'] : "";
		$longitude = is_array($gps) && array_key_exists("longitude", $gps) ? $gps['longitude'] : "";
		$location = get_the_terms( $post_id,'property_location');
		$location = is_array( $location ) ? $location[0]->term_id : "";
		$contract = get_the_terms( $post_id,'contract_type');
		$contract = is_array( $contract ) ? $contract[0]->term_id : "";
		$property = get_the_terms( $post_id,'property_type');
		$property = is_array( $property ) ? $property[0]->term_id : "";
		$amenities_objs = get_the_terms( $post_id,'property_amenities');
		if( is_array( $amenities_objs ) ) {
			foreach ($amenities_objs as $amenities_obj ) {
				array_push($amenities, $amenities_obj->term_id);
			}
		} else {
			$amenities = array();
		}		
	}?>
	<div class="add-property-form">

		<h2><?php echo $form_title;?></h2>

		<form name="dt-property-form" action="" enctype="multipart/form-data" method="post">

			<?php if( isset($_REQUEST['action']) && ( $_REQUEST['action'] === "edit") ) : ?>
				<input type="hidden" name="dt-property-id" value="<?php echo $post_id;?>">
				<input type="hidden" name="dt-property-status" value="<?php echo $post_status;?>">
			<?php endif;?>


			<div class="grey-box-form">

				<div class="column dt-sc-one-column">
					<p>
						<label><?php _e('Title','dt_themes');?><span class="required"> * </span> </label>
						<input type="text" name="dt-property-title" placeholder="<?php __('Property Title','dt_themes');?>" value="<?php echo $title;?>" required>
					</p>
				</div>

                <p class="last dt-sc-one-column">
                    <label><?php _e('Address','dt_themes');?> <span class="required"> * </span> </label>
                    <textarea name="dt-property-address" rows="" cols="" required><?php echo $address;?></textarea>
                </p>
			</div>

			<div class="grey-box-form">

				<p><label><?php _e('Media','dt_themes');?></label></p>

				<div id="dt-frontend-uploader-container">
					<ul id="dt-frontend-uploaded-filelist"><?php
						if( $post_id > 0 ){
							$media = get_post_meta ( $post_id, "_property_media",true);
							$media = is_array($media) ? $media : array();

							if (array_key_exists ( "items", $media )) {

								foreach ( $media ["items_thumbnail"] as $key => $thumbnail ) {
									$item = $media ['items'] [$key];
									$attach_id = $media ['items_id'] [$key];

									$out = "";
									$name = "";

									$foramts = array ( 'jpg', 'jpeg', 'gif', 'png' );
									$parts = explode ( '.', $item );
									$ext = strtolower ( $parts [count ( $parts ) - 1] );

									if (in_array ( $ext, $foramts )) {
										$name = $media ['items_name'] [$key];

										$out .= "<li data-attachment-id='{$attach_id}'>";
										$out .= "<img src='{$thumbnail}' alt='' />";
										$out .= "<span class='dt-image-name'>{$name}</span>";
										$out .= "<input type='hidden' name='items[]' value='{$item}' />";
									} else {
										$name = "video";
										$out .= "<li>";
										$out .= "<span class='dt-video'></span>";
										$out .= "<input type='text' name='items[]' value='{$item}' />";
									}

									$out .= "<input class='dt-image-name' type='hidden' name='items_name[]' value='{$name}' />";
									$out .= "<input type='hidden' name='items_thumbnail[]' value='{$thumbnail}' />";
									$out .= "<input type='hidden' name='items_id[]' value='{$attach_id}' />";
									$out .= "<span class='my_delete'><i class='fa fa-times-circle'> </i></span>";
									$out .= "</li>";
									echo $out;
								}
							}
						}?></ul>
				</div>

				<p>
					<a id="dt-frontend-uploader" href="" title="" class="dt-sc-button small filled with-icon"> <i class="fa fa-upload"> </i><?php _e('Upload Image','dt_themes');?></a>
					<a id="dt-frontend-video-uploader" href="" title="" class="dt-sc-button small filled with-icon"> <i class="fa fa-upload"> </i><?php _e('Upload Video','dt_themes');?></a>
                </p>
			</div>

			<div class="column dt-sc-one-half first">
				<div class="grey-box-form">

					<p>
						<label><?php _e('Featured Video','dt_themes');?></label>
						<input type="text" name="dt-property-video" placeholder="<?php _e('Featured Video','dt_themes');?>" value="<?php echo $property_video;?>">
					</p>

					<p>
						<label><?php _e('Location','dt_themes');?></label>
						<select name="dt-property-location">
							<option value="0"><?php _e('No Location','dt_themes');?></option><?php
								$property_locations = get_categories("taxonomy=property_location&hide_empty=0");
								foreach ( $property_locations as $property_location ) {
									$id = esc_attr( $property_location->term_id );
									$title = esc_html( $property_location->name );
									echo  "<option value='{$id}' " . selected ( $location, $id, false ) . ">{$title}</option>";
								} ?></select>
					</p>

					<p><label><?php _e('GPS Location','dt_themes');?></label></p>

                	<div class="column dt-sc-one-half first">
                		<p>
                			<label><?php _e('Latitude','dt_themes');?></label>
                			<input id="latitude" name="gps[latitude]" type="text" value="<?php echo $latitude;?>"/>
                		</p>
                	</div>

                	<div class="column dt-sc-one-half">
                		<p>
                			<label><?php _e('Longitude','dt_themes');?></label>
                			<input id="longitude" name="gps[longitude]" type="text" value="<?php echo $longitude;?>"/>
                		</p>
                	</div>

                    <p>
                    	<label><?php _e('Contract Type','dt_themes');?></label>
                        <select name="dt-property-contract">
                        	<option value="0"><?php _e('No Contract Type','dt_themes');?></option><?php 
                        	$contract_types = get_categories("taxonomy=contract_type&hide_empty=0");
                        	foreach( $contract_types as $contract_type ){
                        		$id = esc_attr ( $contract_type->term_id );
                        		$title = esc_html ( $contract_type->name );
                        		echo  "<option value='{$id}' " . selected ( $contract, $id, false ) . ">{$title}</option>";
                        		} ?></select>
                    </p>

                    <p class="last">
                        <label><?php _e('Property Type','dt_themes');?></label>
                        <select name="dt-property-type">
                        	<option value="0"><?php _e("No Property Type","dt_themes");?></option><?php
                        	$property_types = get_categories("taxonomy=property_type&hide_empty=0");
                        	foreach ( $property_types as $property_type ) {
                        		$id = esc_attr( $property_type->term_id );
                        		$title = esc_html( $property_type->name );
                        		echo  "<option value='{$id}' " . selected ( $property, $id, false ) . ">{$title}</option>";
                        	} ?></select>
                    </p>
				</div>
			</div>

			<div class="column dt-sc-one-half">
				<div class="grey-box-form">
					<p><label><?php _e('Map','dt_themes');?></label></p>
					<div class="dt-sc-map-container">
					<div id="gmap" style="margin: 0;padding: 0;height: 100%;" data-lat="<?php echo $latitude;?>" data-lng="<?php echo $longitude;?>" data-zoom="<?php echo dttheme_option("property","zoom");?>"></div>
					</div>
				</div>
			</div>

			<div class="column dt-sc-one-half first">
				<div class="grey-box-form">

					<p><label><?php _e('Price in ','dt_themes'); echo dttheme_option("property","currency");?></label>
						<input type="text" name="dt-property-price" placeholder="<?php _e('Property Price','dt_themes');?>" value="<?php echo $price;?>">
					</p>

					<div class="column dt-sc-one-third first">
						<p>
							<label><?php _e('Bed Rooms','dt_themes');?></label>
							<input type="text" name="dt-property-bedrooms" value="<?php echo $bedrooms;?>">
                        </p>
					</div>

                    <div class="column dt-sc-one-third">
                        <p>
                            <label><?php _e('Bath Rooms','dt_themes');?></label>
                            <input type="text" name="dt-property-bathrooms" value="<?php echo $bathrooms;?>">
                        </p>
                    </div>

                    <div class="column dt-sc-one-third">
                        <p>
                            <label><?php _e('Floors','dt_themes');?></label>
                            <input type="text" name="dt-property-floors" value="<?php echo $floors;?>">
                        </p>
                    </div>

					<div class="column dt-sc-one-half first">
						<p>
							<label><?php _e('No.,Parkings','dt_themes');?></label>
							<input type="text" name="dt-property-parkings" value="<?php echo $parking;?>">
                        </p>
                    </div>

                    <div class="column dt-sc-one-half">
						<p>
							<label><?php _e('Area in ','dt_themes'); echo dttheme_option("property","area_unit"); ?></label>
							<input type="text" name="dt-property-area" value="<?php echo $area;?>">
						</p>
					</div>
					
					<p>
						<label><?php _e('Agency','dt_themes');?></label>
                        <select id="dt-select-agency" name="agency">
                        	<option value=""> <?php _e('Select','dt_themes');?></option><?php
                            $agencies = new WP_Query(array('post_type' => 'dt_agencies', 'posts_per_page' => -1, 'order' => 'ASC', 'orderby' => 'title'));
                            if( $agencies->have_posts() ):
                            	while( $agencies->have_posts() ):
                            		$agencies->the_post();
                            		$id = get_the_ID();
                            		$title = get_the_title($id);
                            		$s = selected ( $selected_agency, $id );
                            		echo "<option value='{$id}' {$s}>{$title}</option>";
                            	endwhile;
                            endif;?></select>
					</p>

					<div id="dt-agent-meta" class="last">
						<label><?php _e('Agent','dt_themes');?></label>
                        <?php $agents = get_post_meta ( $post_id, "_property_agents",true);
							  $agents = is_array($agents) ? array_unique($agents) : array();
							  $agent = !empty($agents) ? $agents[0] : ""; ?>
						<select id="dt-select-agent" name="agents[]" data-selected-agent="<?php echo $agent;?>">
							<option value=""><?php _e("No Agent",'dt_themes');?></option>
						</select>							
					</div>
                </div>
			</div>

			<div class="column dt-sc-one-half">
				<div class="grey-box-form">
					<p><label><?php _e('Amenities','dt_themes');?></label></p>
					<div class="column dt-sc-one-half first checkbox-group"><?php
						$property_amenities = get_categories("taxonomy=property_amenities&hide_empty=0");
						foreach( $property_amenities as $property_amenitie ) {
							$id = esc_attr( $property_amenitie->term_id );
							$title = esc_html( $property_amenitie->name );
							$checked = in_array($id, $amenities) ? " checked " : "";
							echo "<label><input type='checkbox' name='dt-property-amenities[]' value='{$id}' {$checked} /> {$title}</label>";
						}?>
					</div>
				</div>
			</div>

			<div class="grey-box-form">
				<p class="last">
					<?php wp_editor($content,'dt-property-content-area',array('textarea_name' => 'dt-property-content-area', 'teeny' => false, 'textarea_rows' => 8));?>
				</p>
			</div>

			<input type="submit" name="dt-propery-submit" value="<?php echo $submit;?>">

		</form>
	</div>	
<?php }

function dt_property_form_result( $msg ) { ?>
	<div class="column dt-sc-one-half space first">
    </div>
    
	<div class="column dt-sc-one-half space">
    	<div class="dt-sc-success-box"> <?php echo esc_html( $msg); ?></div>
    </div>
<?php
}
?>