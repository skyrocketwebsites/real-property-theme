<?php $properties = array(
		'post_type'=>'dt_properties',
		'posts_per_page' => '-1',
		'meta_query'=>array( array('key' => '_show_in_map','value' => 'true')),
		'order_by'=> 'published');

		$query = new WP_Query($properties);
		if( $query->have_posts() ):
			$out =  '<script type="text/javascript">';
			$out .= 'var knowmore ="'.__('Know More','dt_themes').'";';
			$out .= ' var properties = [';
			$data = "";
			while ( $query->have_posts() ):
				$query->the_post();

				$the_id = get_the_ID();
				$permalink = get_permalink($the_id);
				$title = get_the_title($the_id);

				$media = get_post_meta ( $the_id, "_property_media",true);
				$media = is_array($media) ? $media : array();

				if( array_key_exists("items_id", $media) ) {
					$items = array_unique( $media['items_id']);
					if( array_key_exists("video",$items)) {
						unset( $items[array_search("video", $items)]);
					}	
					$img = $media['items'][key($items)];
				}else{
					$img = "http://placehold.it/1060x718&text=Real Home";
				}


				$gps = get_post_meta ( $the_id, "_property_gps",true);
				$gps = is_array($gps) ? $gps : array();
				$latitude = array_key_exists("latitude", $gps) ? $gps['latitude'] : "";
				$longitude = array_key_exists("longitude", $gps) ? $gps['longitude'] : "";
				
				$price = get_post_meta ( $the_id, "_property_price",true);
				$currency = dttheme_option("property","currency");
				$price = $currency.$price;

				$property_type = get_the_terms( $the_id, 'property_type' );
				if( is_object( $property_type) || is_array($property_type) ){
					foreach ( $property_type as $c ) :
						$property_type = $c->slug;
					    $icon = get_option( "taxonomy_term_$c->term_id" );
					    $icon = $icon['icon'];
					    $icon = !empty( $icon ) ? $icon : get_template_directory_uri().'/images/default-marker.png';
					endforeach;
				} else {
					$icon = get_template_directory_uri().'/images/default-marker.png';
				}
				$data .=   "{ id:'{$id}',title:'{$title}',link:'{$permalink}',latitude:{$latitude},longitude:{$longitude},thumb:'{$img}',price:'{$price}', icon:'{$icon}' },";
			endwhile;
			$data = trim($data, ",");
			$out .= $data;
			$out.= '];';
			$out.= '</script>';
			echo $out;
		endif;
		wp_reset_query();?>
<script type="text/javascript">

	jQuery(document).ready(function() {

   		/* function to attach info window with marker */
    	function attachInfoWindowToMarker( map, marker, infoWindow ){
    		google.maps.event.addListener( marker, 'click', function(){
    			infoWindow.open( map, marker );
    		});
   		}


		if( jQuery("div#slider").length ) {

			var $lat = jQuery("div#slider").data("lat");
			var $lng = jQuery("div#slider").data("lng");	
			var $zoom = jQuery("div#slider").data("zoom");

			var myLatlng = new google.maps.LatLng($lat,$lng);
			var mapOptions = { zoom:$zoom, scrollwheel:false , center:myLatlng } 
			var map = new google.maps.Map(document.getElementById('slider'), mapOptions);

			/* Property */
			if( typeof properties !== 'undefined' ) {
			var markers = new Array();
			var info_windows = new Array();
			for (var i=0; i < properties.length; i++) {
				markers[i] = new google.maps.Marker({
					position: new google.maps.LatLng(properties[i].latitude,properties[i].longitude),
					map:map,
					title: properties[i].title,
					icon: properties[i].icon
			 	});

			 	info_windows[i] = new google.maps.InfoWindow({
			 		content: '<div class="top-map-info-window">'+
			 		'<a class="thumb" href="'+properties[i].link+'"><img class="thumb" src="'+properties[i].thumb+'" alt="'+properties[i].title+'"/><span class="map-pro-price">'+properties[i].price+'</span></a>'+
			 		'<h5 class="map-pro-title"><a href="'+properties[i].link+'">'+properties[i].title+'</a></h5>'+					
			 		'<a class="dt-sc-button filled small" href="'+properties[i].link+'">'+knowmore+'</a>'+
			 		'</div>'

			 	});

			 	attachInfoWindowToMarker(map, markers[i], info_windows[i]);
			}
			}/* Property End*/
    	}
	});
</script>
<div id="slider" style="height:600px;" data-lat="<?php echo dttheme_option("property","latitude");?>" data-lng="<?php echo dttheme_option("property","longitude");?>" data-zoom="<?php echo dttheme_option("property","zoom");?>">
</div>