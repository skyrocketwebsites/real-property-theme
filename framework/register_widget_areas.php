<?php 
	#Display Everywhere
	register_sidebar(array(
		'name' 			=>	'Display Everywhere',
		'id'			=>	'display-everywhere-sidebar',
		'description'	=>	__("Common sidebar that appears on the left (or) right.","dt_themes"),
		'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
		'after_widget' 	=> 	'</aside>',
		'before_title' 	=> 	'<h3 class="widgettitle">',
		'after_title' 	=> 	'<span></span></h3>'));

	#Properties Archive Sidebars
	$page_layout  = dttheme_option( 'property', 'property-archive-page-layout' );
	$page_layout  = !empty( $page_layout ) ? $page_layout : "content-full-width";
	if( $page_layout !== "content-full-width" ) {
		register_sidebar(array(
			'name' 			=>	"Properties Archive Sidebar",
			'id'			=>	"properties-archive-sidebar",
			'description'	=> __("Sidebar that appears on the agents archive page.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	}

	#Agents Archive Sidebars
	$page_layout  = dttheme_option( 'property', 'agent-archive-page-layout' );
	$page_layout  = !empty( $page_layout ) ? $page_layout : "content-full-width";
	if( $page_layout !== "content-full-width" ) {
		register_sidebar(array(
			'name' 			=>	"Agents Archive Sidebar",
			'id'			=>	"agents-archive-sidebar",
			'description'	=> __("Sidebar that appears on the agents archive page.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	}

	#Agencies Archive Sidebars
	$page_layout  = dttheme_option( 'property', 'agency-archive-page-layout' );
	$page_layout  = !empty( $page_layout ) ? $page_layout : "content-full-width";
	if( $page_layout !== "content-full-width" ) {
		register_sidebar(array(
			'name' 			=>	"Agencies Archive Sidebar",
			'id'			=>	"agencies-archive-sidebar",
			'description'	=> __("Sidebar that appears on the agencies archive page.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	}

	#Footer Columnns		
	$footer_columns =  dttheme_option('general','footer-columns');
	dttheme_footer_widgetarea($footer_columns);
	
	#Custom LEFT sidebars for Pages
	$page = dttheme_option("widgetarea","left-pages");	
	$page = !empty($page) ? $page : array();
	$widget_areas_for_pages = array_filter(array_unique($page));
	foreach($widget_areas_for_pages as $page_id):
		$title = get_the_title($page_id);	
		register_sidebar(array(
			'name' 			=>	"Page: {$title} - Left",
			'id'			=>	"left-page-{$page_id}-sidebar",
			'description'	=> __("Individual page sidebar that appears on the left.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;

	#Custom RIGHT sidebars for Pages
	$page = dttheme_option("widgetarea","right-pages");	
	$page = !empty($page) ? $page : array();
	$widget_areas_for_pages = array_filter(array_unique($page));
	foreach($widget_areas_for_pages as $page_id):
		$title = get_the_title($page_id);	
		register_sidebar(array(
			'name' 			=>	"Page: {$title} - Right",
			'id'			=>	"right-page-{$page_id}-sidebar",
			'description'	=> __("Individual page sidebar that appears on the right.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;
	
	#Custom Left sidebars for Posts
	$posts = dttheme_option("widgetarea","left-posts");
	$posts = !empty($posts) ? $posts : array();
	$widget_areas_for_posts = array_filter(array_unique($posts));
	foreach($widget_areas_for_posts as $post_id):
		$title = get_the_title($post_id);	
		register_sidebar(array(
			'name' 			=>	"Post: {$title} - Left",
			'id'			=>	"left-post-{$post_id}-sidebar",
			'description'	=> __("Individual post sidebar that appears on the left.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;

	#Custom Right sidebars for Posts
	$posts = dttheme_option("widgetarea","right-posts");
	$posts = !empty($posts) ? $posts : array();
	$widget_areas_for_posts = array_filter(array_unique($posts));
	foreach($widget_areas_for_posts as $post_id):
		$title = get_the_title($post_id);	
		register_sidebar(array(
			'name' 			=>	"Post: {$title} - Right",
			'id'			=>	"right-post-{$post_id}-sidebar",
			'description'	=> __("Individual post sidebar that appears on the right.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;

	#Custom Left sidebars for categories 
	$cats = dttheme_option("widgetarea","left-cats");
	$cats = !empty($cats) ? $cats : array();
	$widget_areas_for_cats = array_filter(array_unique($cats));
	foreach($widget_areas_for_cats as $cat_id):
		$title = get_the_category_by_ID($cat_id);
		register_sidebar(array(
			'name' 			=>	"Category: {$title} - Left",
			'id'			=>	"left-category-{$cat_id}-sidebar",
			'description'	=> __("Individual category sidebar that appears on the left.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;

	#Custom Right sidebars for categories 
	$cats = dttheme_option("widgetarea","right-cats");
	$cats = !empty($cats) ? $cats : array();
	$widget_areas_for_cats = array_filter(array_unique($cats));
	foreach($widget_areas_for_cats as $cat_id):
		$title = get_the_category_by_ID($cat_id);
		register_sidebar(array(
			'name' 			=>	"Category: {$title} - Right",
			'id'			=>	"right-category-{$cat_id}-sidebar",
			'description'	=> __("Individual category sidebar that appears on the right.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;

	#Custom Left Sidebar for dt_properties
	$dt_properties = dttheme_option("widgetarea","left-dt_properties");
	$dt_properties = !empty($dt_properties) ? $dt_properties : array();
	$widget_areas_for_dt_properties = array_filter(array_unique($dt_properties));
	foreach($widget_areas_for_dt_properties as $post_id):
		$title = get_the_title($post_id);	
		register_sidebar(array(
			'name' 			=>	"Property : {$title} - Left",
			'id'			=>	"left-dt_properties-{$post_id}-sidebar",
			'description'	=> __("Individual property sidebar that appears on the left.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;

	#Custom Right Sidebar for dt_properties
	$dt_properties = dttheme_option("widgetarea","right-dt_properties");
	$dt_properties = !empty($dt_properties) ? $dt_properties : array();
	$widget_areas_for_dt_properties = array_filter(array_unique($dt_properties));
	foreach($widget_areas_for_dt_properties as $post_id):
		$title = get_the_title($post_id);	
		register_sidebar(array(
			'name' 			=>	"Property : {$title} - right",
			'id'			=>	"right-dt_properties-{$post_id}-sidebar",
			'description'	=> __("Individual property sidebar that appears on the right.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;


	#Custom Left Sidebar for dt_agents
	$dt_agents = dttheme_option("widgetarea","left-dt_agents");
	$dt_agents = !empty($dt_agents) ? $dt_agents : array();
	$widget_areas_for_dt_agents = array_filter(array_unique($dt_agents));
	foreach($widget_areas_for_dt_agents as $post_id):
		$title = get_the_title($post_id);	
		register_sidebar(array(
			'name' 			=>	"Agent : {$title} - Left",
			'id'			=>	"left-dt_agents-{$post_id}-sidebar",
			'description'	=> __("Individual agent sidebar that appears on the left.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;
	
	#Custom Right Sidebar for dt_agents
	$dt_agents = dttheme_option("widgetarea","right-dt_agents");
	$dt_agents = !empty($dt_agents) ? $dt_agents : array();
	$widget_areas_for_dt_agents = array_filter(array_unique($dt_agents));
	foreach($widget_areas_for_dt_agents as $post_id):
		$title = get_the_title($post_id);	
		register_sidebar(array(
			'name' 			=>	"Agent : {$title} - right",
			'id'			=>	"right-dt_agents-{$post_id}-sidebar",
			'description'	=> __("Individual agent sidebar that appears on the right.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;


	#Custom Left Sidebar for dt_agencies
	$dt_agencies = dttheme_option("widgetarea","left-dt_agencies");
	$dt_agencies = !empty($dt_agencies) ? $dt_agencies : array();
	$widget_areas_for_dt_agencies = array_filter(array_unique($dt_agencies));
	foreach($widget_areas_for_dt_agencies as $post_id):
		$title = get_the_title($post_id);	
		register_sidebar(array(
			'name' 			=>	"Agency : {$title} - Left",
			'id'			=>	"left-dt_agencies-{$post_id}-sidebar",
			'description'	=> __("Individual agent sidebar that appears on the left.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;
	
	#Custom Right Sidebar for dt_agencies
	$dt_agencies = dttheme_option("widgetarea","right-dt_agencies");
	$dt_agencies = !empty($dt_agencies) ? $dt_agencies : array();
	$widget_areas_for_dt_agencies = array_filter(array_unique($dt_agencies));
	foreach($widget_areas_for_dt_agencies as $post_id):
		$title = get_the_title($post_id);	
		register_sidebar(array(
			'name' 			=>	"Agency : {$title} - right",
			'id'			=>	"right-dt_agencies-{$post_id}-sidebar",
			'description'	=> __("Individual agent sidebar that appears on the right.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;
	

if( class_exists('woocommerce')	):

	#Shop Everywhere Sidebar
	register_sidebar(array(
		'name' 			=>	'Shop Everywhere',
		'id'			=>	'shop-everywhere-sidebar',
		'description'   =>  __("Shop page unique sidebar that appears on the left (or) right.","dt_themes"),
		'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
		'after_widget' 	=> 	'</aside>',
		'before_title' 	=> 	'<h3 class="widgettitle">',
		'after_title' 	=> 	'<span></span></h3>'));

	#Custom Left Sidebars for Product
	$products = dttheme_option("widgetarea","left-products");
	$products = !empty($products) ? $products : array();
	$widget_areas_for_products = array_filter(array_unique($products));
	foreach($widget_areas_for_products as $id):
		$title = get_the_title($id);
		register_sidebar(array(
			'name' 			=>	"Product: {$title} - Left",
			'id'			=>	"left-product-{$id}-sidebar",
			'description'	=> __("Individual product sidebar that appears on the left.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;

	#Custom Right Sidebars for Product
	$products = dttheme_option("widgetarea","right-products");
	$products = !empty($products) ? $products : array();
	$widget_areas_for_products = array_filter(array_unique($products));
	foreach($widget_areas_for_products as $id):
		$title = get_the_title($id);
		register_sidebar(array(
			'name' 			=>	"Product: {$title} - Right",
			'id'			=>	"right-product-{$id}-sidebar",
			'description'	=> __("Individual product sidebar that appears on the right.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;


	#Custom Left Sidebars for Product Category
	$product_categories = dttheme_option("widgetarea","left-product-category");
	$product_categories = !empty($product_categories) ? $product_categories : array();
	$widget_areas_for_product_categories = array_filter(array_unique($product_categories));
	
	foreach($widget_areas_for_product_categories as $id):
	
		$title = $wpdb->get_var( $wpdb->prepare("SELECT name FROM $wpdb->terms  WHERE term_id = %s",$id));
		$slug  = $wpdb->get_var( $wpdb->prepare("SELECT slug FROM $wpdb->terms  WHERE term_id = %s",$id));	
		
		register_sidebar(array(
			'name' 			=>	"Product Category: {$title} - Left ",
			'id'			=>	"left-product-category-{$slug}-sidebar",
			'description'	=> __("Individual product category sidebar that appears on the left.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;
	
	#Custom Right Sidebars for Product Category
	$product_categories = dttheme_option("widgetarea","right-product-category");
	$product_categories = !empty($product_categories) ? $product_categories : array();
	$widget_areas_for_product_categories = array_filter(array_unique($product_categories));
	
	foreach($widget_areas_for_product_categories as $id):
	
		$title = $wpdb->get_var( $wpdb->prepare("SELECT name FROM $wpdb->terms  WHERE term_id = %s",$id));
		$slug  = $wpdb->get_var( $wpdb->prepare("SELECT slug FROM $wpdb->terms  WHERE term_id = %s",$id));	
		
		register_sidebar(array(
			'name' 			=>	"Product Category: {$title} - Right ",
			'id'			=>	"right-product-category-{$slug}-sidebar",
			'description'	=> __("Individual product category sidebar that appears on the right.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;

	#Custom Left Sidebars for Product Tag
	$product_tags = dttheme_option("widgetarea","left-product-tag");
	$product_tags = !empty($product_tags) ? $product_tags : array();
	$widget_areas_for_product_tags = array_filter(array_unique($product_tags));
	foreach($widget_areas_for_product_tags as $id):
		$title = $wpdb->get_var( $wpdb->prepare("SELECT name FROM $wpdb->terms  WHERE term_id = %s",$id));
		$slug  = $wpdb->get_var( $wpdb->prepare("SELECT slug FROM $wpdb->terms  WHERE term_id = %s",$id));	
		register_sidebar(array(
			'name' 			=>	"Product Tag: {$title} - Left",
			'id'			=>	"left-product-tag-{$slug}-sidebar",
			'description'	=> __("Individual product tag sidebar that appears on the left.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;

	#Custom Right Sidebars for Product Tag
	$product_tags = dttheme_option("widgetarea","right-product-tag");
	$product_tags = !empty($product_tags) ? $product_tags : array();
	$widget_areas_for_product_tags = array_filter(array_unique($product_tags));
	foreach($widget_areas_for_product_tags as $id):
		$title = $wpdb->get_var( $wpdb->prepare("SELECT name FROM $wpdb->terms  WHERE term_id = %s",$id));
		$slug  = $wpdb->get_var( $wpdb->prepare("SELECT slug FROM $wpdb->terms  WHERE term_id = %s",$id));	
		register_sidebar(array(
			'name' 			=>	"Product Tag: {$title} - Right",
			'id'			=>	"right-product-tag-{$slug}-sidebar",
			'description'	=> __("Individual product tag sidebar that appears on the right.","dt_themes"),
			'before_widget' => 	'<aside id="%1$s" class="widget %2$s">',
			'after_widget' 	=> 	'</aside>',
			'before_title' 	=> 	'<h3 class="widgettitle">',
			'after_title' 	=> 	'<span></span></h3>'));
	endforeach;
endif;?>