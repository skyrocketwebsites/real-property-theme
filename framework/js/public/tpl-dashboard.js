jQuery(document).ready(function(){
	
	jQuery('form[name="dt-property-form"]').validate();
	
	/*jQuery('form[name="dt-property-form"]').submit(function(){
		alert("hi");
		return false;
	});*/

	jQuery('body').delegate('span.my_delete','click', function(){
		$li = jQuery(this).parent('li');
		var $id = $li.attr("data-attachment-id");
		jQuery.ajax({
			type: "post",
			url : ajaxurl,
			data:{ action:'dt_themes_ajax_call','type':'dt_remove_media', 'media_id': $id},
			complete: function(response){
				if( response.status === 200 ) {
					$li.remove();
				}
			}
		});
	});


	//Fill Agent
	jQuery("select#dt-select-agency").change(function(){
		$val = jQuery(this).val();
		$selected_agent = jQuery("select#dt-select-agent").data("selected-agent");
		jQuery("div#dt-agent-meta").slideUp();

		jQuery.ajax({
			type : "post",
			url : ajaxurl,
			data : { action:'dt_themes_ajax_call','type': "dttheme_fill_agent_by_agency", 'agency_id' : $val , "selected_agent":$selected_agent },
			complete: function(response){
				var $append = "";
				if( response.status === 200 ) {
					if( jQuery.trim(response.responseText).length > 0 ) {
						$append += '<option value="">Select</option>';
						$append += response.responseText;
					}else {
						$append += '<option value="">No Agents</option>';
					}

					if( jQuery.trim($val).length > 0  ){
						jQuery("div#dt-agent-meta").slideDown('slow',function(){
							jQuery("select#dt-select-agent").empty().append($append);		
						});
					}
				}
			}
		});
	});
	jQuery("select#dt-select-agency").trigger('change');
	//Fill Agent End
	

	//Up loader Starts
		var uploader = new plupload.Uploader(dt_plupload.plupload);
		uploader.init();

		jQuery("#dt-frontend-uploader").click(function(e) {
			uploader.start();
	        e.preventDefault();
	    });

	    uploader.bind('FilesAdded', function(up, files){
	    	jQuery.each(files, function(i, file) {});
	    	up.refresh(); // Reposition Flash/Silverlight
	        uploader.start();
		});

		uploader.bind('UploadProgress', function(up, file) {});

		uploader.bind('Error', function(up, err) {
			console.log(err);
			up.refresh(); // Reposition Flash/Silverlight
		});

		uploader.bind('FileUploaded', function(up, file, response) {

			var resp = jQuery.parseJSON(response.response);

			if( resp.success ) {
				jQuery('#dt-frontend-uploaded-filelist').append(resp.html);
			}

			if(resp.success === "file_type" ){
				jQuery('#dt-frontend-uploaded-filelist').append("<li class='error'>"+resp.message+"</li>");	
			}

			if(resp.success === "file_size" ){
				jQuery('#dt-frontend-uploaded-filelist').append("<li class='error'>"+resp.message+"</li>");	
			}
	    });
    //Up loader Ends Here

	//Video Up loader
	jQuery("#dt-frontend-video-uploader").click(function(e){
		var $video =  "<li>" +
			"<span class='dt-video'></span>" +
			"<input type='text' name='items[]' value='http://vimeo.com/18439821'/>" +
			"<input type='hidden' class='dt-image-name' name='items_name[]' value='video' />" +
			"<input type='hidden' name='items_thumbnail[]' value='http://vimeo.com/18439821' />" +
			"<input type='hidden' name='items_id[]' value='video' />"+
			"<span class='my_delete'><i class='fa fa-times-circle'> </i></span>" +
			"</li>";

		jQuery('#dt-frontend-uploaded-filelist').append($video);
		e.preventDefault();
	});//Video Up loader End

    jQuery('ul#dt-frontend-uploaded-filelist').sortable({
      placeholder: 'sortable-placeholder',
      forcePlaceholderSize: true,
      cancel: '.my_delete, input, textarea, label'
    });
	

	if( jQuery("div#gmap").length ){

		var lat = jQuery("div#gmap").data("lat");
			lat = ( jQuery.trim(lat).length > 0  ) ? lat :  "-37.80544394934272";

		var lng = jQuery("div#gmap").data("lng");	
			lng = ( jQuery.trim(lng).length > 0 ) ? lng : "144.964599609375";

		var zoom = jQuery("div#gmap").data("zoom");
			zoom = ( jQuery.trim(zoom).length > 0 ) ? zoom : "14";

		var myLatlng = new google.maps.LatLng(lat, lng);
		var mapOptions = {
			zoom: zoom,
			center: myLatlng,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		}

		var map = new google.maps.Map(document.getElementById('gmap'), mapOptions);

		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map,
			draggable: true
		});

		google.maps.event.addListener(marker, 'dragend', function(){ 
			var position = marker.getPosition();
			var lat = position.lat();
			jQuery('#latitude').attr('value', lat);
			var lon = position.lng();
			jQuery('#longitude').attr('value', lon);
		});	
	}

});