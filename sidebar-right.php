<?php
wp_reset_query();
global $post;

if( ("product" === get_post_type()) and (is_post_type_archive('product')) ):
	dttheme_display_everywhere_sidebar('page',get_option('woocommerce_shop_page_id'));
	dttheme_shop_sidebar('right-page-'.get_option('woocommerce_shop_page_id').'-sidebar');

elseif( is_post_type_archive('dt_agents') ):
	if(function_exists('dynamic_sidebar') && dynamic_sidebar(('agents-archive-sidebar')) ):  endif;

elseif( is_post_type_archive('dt_agencies') ):
	if(function_exists('dynamic_sidebar') && dynamic_sidebar(('agencies-archive-sidebar')) ):  endif;

elseif( is_post_type_archive('dt_properties') || is_tax('property_location') || is_tax('property_type') || is_tax('contract_type') ):
	if(function_exists('dynamic_sidebar') && dynamic_sidebar(('properties-archive-sidebar')) ):  endif;

elseif( class_exists('woocommerce') && is_product_category() ): 
	dttheme_shop_sidebar('right-product-category-'.get_query_var('product_cat').'-sidebar');

elseif( class_exists('woocommerce') && is_product_tag() ):
	dttheme_shop_sidebar('right-product-tag-'.get_query_var('product_tag').'-sidebar');

elseif( is_singular('product') ):
	dttheme_shop_sidebar('right-product-'.$post->ID.'-sidebar');

elseif(class_exists('woocommerce') && ( is_cart() || is_checkout() || is_order_received_page() || is_account_page() )):
	dttheme_display_everywhere_sidebar('page',$post->ID);
	dttheme_shop_sidebar('right-page-'.$post->ID.'-sidebar');
	
elseif( is_category() ):

	if(function_exists('dynamic_sidebar') && dynamic_sidebar(('right-category-'.get_query_var('cat').'-sidebar')) ):
	else:
		if(function_exists('dynamic_sidebar') && dynamic_sidebar(('display-everywhere-sidebar')) ): endif;	
	endif;

elseif( is_page() || ( is_single() and 'post'== get_post_type() ) ) :

	if( is_page() ):
		dttheme_display_everywhere_sidebar('page',$post->ID);
		if(function_exists('dynamic_sidebar') && dynamic_sidebar(('right-page-'.get_the_ID().'-sidebar')) ):  endif;

	elseif( is_single() and "post" === get_post_type() ):
		dttheme_display_everywhere_sidebar('post',$post->ID);
		if(function_exists('dynamic_sidebar') && dynamic_sidebar(('right-post-'.get_the_ID().'-sidebar')) ):  endif;
	endif;

elseif( is_single() and "dt_properties" === get_post_type() ):	
		dttheme_display_everywhere_sidebar('dt_properties',$post->ID);
		if(function_exists('dynamic_sidebar') && dynamic_sidebar(('right-dt_properties-'.get_the_ID().'-sidebar')) ):  endif;

elseif( is_single() and "dt_agents" === get_post_type() ):
		dttheme_display_everywhere_sidebar('dt_agents',$post->ID);
		if(function_exists('dynamic_sidebar') && dynamic_sidebar(('right-dt_agents-'.get_the_ID().'-sidebar')) ):  endif;

elseif( is_single() and "dt_agencies" === get_post_type() ):	
		dttheme_display_everywhere_sidebar('dt_agencies',$post->ID);
		if(function_exists('dynamic_sidebar') && dynamic_sidebar(('right-dt_agencies-'.get_the_ID().'-sidebar')) ):  endif;
else:
	if(function_exists('dynamic_sidebar') && dynamic_sidebar(('display-everywhere-sidebar')) ): endif;
endif;?>