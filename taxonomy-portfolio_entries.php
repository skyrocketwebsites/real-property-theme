<?php get_header();

	$page_layout 	= dttheme_option('specialty','archives-layout');
  	$page_layout 	= !empty($page_layout) ? $page_layout : "content-full-width";
	$show_sidebar = $show_left_sidebar = $show_right_sidebar =  false;
	$sidebar_class = "";

	switch ( $page_layout ) {
		case 'with-left-sidebar':
			$page_layout = "page-with-sidebar with-left-sidebar";
			$show_sidebar = $show_left_sidebar = true;
			$sidebar_class = "secondary-has-left-sidebar";
		break;

		case 'with-right-sidebar':
			$page_layout = "page-with-sidebar with-right-sidebar";
			$show_sidebar = $show_right_sidebar	= true;
			$sidebar_class = "secondary-has-right-sidebar";
		break;

		case 'both-sidebar':
			$page_layout = "page-with-sidebar page-with-both-sidebar";
			$show_sidebar = $show_right_sidebar	= $show_left_sidebar = true;
			$sidebar_class = "secondary-has-both-sidebar";
		break;

		case 'content-full-width':
		default:
			$page_layout = "content-full-width";
		break;
	}

	if ( $show_sidebar ):
		if ( $show_left_sidebar ): ?>
			<!-- Secondary Left -->
			<section id="secondary-left" class="<?php echo esc_attr($sidebar_class);?>">
				<?php get_sidebar( 'left' );?>
			</section><?php
		endif;
	endif;?>
	<!-- ** Primary Section ** -->
	<section id="primary" class="<?php echo esc_attr($page_layout);?>"><?php

		$post_layout = dttheme_option('specialty','archives-post-layout'); 
		$post_layout = !empty($post_layout) ? $post_layout : "one-column";
		$post_class = "";

		#TO SET POST LAYOUT
		switch($post_layout):

			case 'one-column':
				$post_class = $show_sidebar ? " portfolio column dt-sc-one-column with-sidebar" : " portfolio column dt-sc-one-column ";
			break;

			case 'one-half-column';
				$post_class = $show_sidebar ? " portfolio column dt-sc-one-half with-sidebar " : " portfolio column dt-sc-one-half ";
				$columns = 2;
			break;

			case 'one-third-column':
				$post_class = $show_sidebar ? " portfolio column dt-sc-one-third with-sidebar " : " portfolio column dt-sc-one-third ";
				$columns = 3;
			break;

			case 'one-fourth-column':
				$post_class = $show_sidebar ? " portfolio column dt-sc-one-fourth with-sidebar " : "portfolio column dt-sc-one-fourth";
				$columns = 4;
			break;
		endswitch;?>

		<!-- **Portfolio Container** -->
		<div class="dt-sc-portfolio-container gallery"><?php
			if( have_posts() ):
				$i = 1;
				while( have_posts() ):
					the_post();
					$the_id = get_the_ID();
					$portfolio_item_meta = get_post_meta($the_id,'_portfolio_settings',TRUE);
					$portfolio_item_meta = is_array($portfolio_item_meta) ? $portfolio_item_meta  : array();

					$temp_class = "";
					if($i == 1) $temp_class = $post_class." first"; else $temp_class = $post_class;
					if($i == $columns) $i = 1; else $i = $i + 1;?>

                    <!-- Portfolio Item -->
                    <div id="portfolio-<?php echo esc_attr($the_id);?>" class="<?php echo esc_attr($temp_class);?>">
                    	<figure>
                    		<?php $popup = "http://placehold.it/1060x795&text=Add%20Image%20/%20Video%20%20to%20Portfolio";
                    		if( array_key_exists('items_name', $portfolio_item_meta) ) {
                    			$item =  $portfolio_item_meta['items_name'][0];
                    			$popup = $portfolio_item_meta['items'][0];

                    			if( "video" === $item ) {
                    				$items = array_diff( $portfolio_item_meta['items_name'] , array("video") );

                    				if( !empty($items) ) {
                    					echo "<img src='".esc_attr($portfolio_item_meta['items'][key($items)])."' width='1060' height='795' />";	
                        			} else {
                        				echo '<img src="http://placehold.it/1060x795&text=Add%20Image%20/%20Video%20%20to%20Portfolio" width="1060" height="795"/>';
                        			}
                        		} else {
                        			echo "<img src='".esc_attr($portfolio_item_meta['items'][0])."' width='1060' height='795'/>";
                        		}
                        	} else {
                        		echo "<img src='".esc_attr($popup)."'/>";
                        	}?>
                        	<figcaption>
                        		<div class="fig-title">
                        			<h5><a href="<?php the_permalink();?>" title="<?php printf( esc_attr__('%s'), the_title_attribute('echo=0'));?>"><?php the_title();?></a></h5>

                        			<?php if( array_key_exists("sub-title",$portfolio_item_meta) ): ?>
                        				<h6><?php echo esc_html($portfolio_item_meta["sub-title"]);?></h6>
                        			<?php endif;?>
                        		</div>
                        		<div class="fig-overlay">
                        			<a href="<?php echo esc_attr($popup);?>" data-gal="prettyPhoto[gallery]" class="zoom" title=""> <span class="fa fa-plus"> </span> </a>
                        			<a href="<?php the_permalink();?>" class="link" title=""> <span class="fa fa-link"> </span> </a>
                        		</div>
                        	</figcaption>                        		
                        </figure>                    
                    </div><!-- Portfolio Item End-->

		  <?php endwhile;
			else: ?>
				<div class="dt-sc-hr-invisible"> </div>
				<h1><?php _e( 'Nothing Found','dt_themes'); ?></h1>
				<h3><?php _e( 'Apologies, but no results were found for the requested archive.', 'dt_themes'); ?></h3>
				<?php get_search_form();?>
	<?php	endif;?>			
		</div><!-- **Portfolio Container** -->

        <div class="dt-sc-clear"></div>
        <div class="dt-sc-hr-invisible"> </div>

		<!-- **Pagination** -->
		<div class="pagination">
			<div class="prev-post"><?php previous_posts_link('<span class="fa fa-angle-double-left"></span> Prev');?></div>
			<?php echo dttheme_pagination();?>
			<div class="next-post"><?php next_posts_link('Next <span class="fa fa-angle-double-right"></span>');?></div>
		</div><!-- **Pagination - End** -->



	</section><!-- ** Primary Section End ** --><?php

	if ( $show_sidebar ):
		if ( $show_right_sidebar ): ?>
			<!-- Secondary Right -->
			<section id="secondary-right" class="<?php echo esc_attr($sidebar_class);?>"><?php get_sidebar( 'right' );?></section><?php
		endif;
	endif;?>
<?php get_footer(); ?>		