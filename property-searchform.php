<!-- **Property Search Container** -->
<div class="property-search-container">
    <div class="dt-sc-tabs-container">
        <ul class="dt-sc-tabs-frame"><?php

            $action = dt_get_page_permalink_by_its_template('tpl-property-search.php');
            $property = dttheme_option("property");
			$contract_types = array( "default" =>"Default");
			
            $terms = get_terms( "contract_type");
			foreach ($terms as $term ) {
				$contract_types[$term->term_id] = $term->name;
			}
			
			foreach( $contract_types as $key => $value ) {
				
				if( dttheme_is_plugin_active('sitepress-multilingual-cms/sitepress.php') ) {
					
					# To get main language contact type key based on current language contract type key
					$t = get_term_by('id',$key,'contract_type');
					if($t){
						global $sitepress;
						$key =  icl_object_id( $t->term_id,'contract_type', true ,$sitepress->get_default_language() );
					}
					#End
				}
				
				
                if( array_key_exists("enable-{$key}-search", $property ) ):
				
					if( dttheme_is_plugin_active('sitepress-multilingual-cms/sitepress.php') ) {
							$lang = ICL_LANGUAGE_CODE;
							$title = dttheme_option("property","{$lang}-{$key}-title");
                            $title = dt_wp_kses( $title ); 							
							
						echo '<li><a href="#">'.$title.'</a></li>';
					} else {
                            $title = dttheme_option("property","{$key}-title");
                            $title = dt_wp_kses( $title );                          
						echo "<li><a href='#'>".$title.'</a></li>';
					}
                endif;
            }?></ul>

            <?php foreach( $contract_types as $key => $value ) {
				
					if( dttheme_is_plugin_active('sitepress-multilingual-cms/sitepress.php') ) {
						
						# To get main language contact type key based on current language contract type key
						$t = get_term_by('id',$key,'contract_type');
						if($t){
							global $sitepress;
							$key =  icl_object_id( $t->term_id,'contract_type', true ,$sitepress->get_default_language() );
						}
						#End
					}
						
                if( array_key_exists("enable-{$key}-search", $property ) ):
                    echo '<div class="dt-sc-tabs-frame-content">';
                    echo "<form action='{$action}' method='get'>";

                    #Hidden
					$ckey = $key;
					if( dttheme_is_plugin_active('sitepress-multilingual-cms/sitepress.php') ) {
						$ckey = icl_object_id( $key ,'contract_type', true , ICL_LANGUAGE_CODE );
					}
                    echo "<input type='hidden' name='searchby' value='".esc_attr($ckey)."'>";

                    #Title Module
                    if( array_key_exists("enable-title-module-for-{$key}", $property) ):
                        echo '<div class="title-module large-module">';                        
                        echo '<label>'.__("Name","dt_themes").'</label>';
                        echo '<input type="text" name="pname"/>';
                        echo '</div>';
                    endif;

                    #Property Type Module
                    if( array_key_exists("enable-property-type-module-for-{$key}", $property) ):
                        echo '<div class="property-type-module medium-module">';
                        echo '<label>'.__('Type','dt_themes').'</label>';
                        echo '<select name="ptype">';
                        echo '<option value="0">'.__("Property Type","dt_themes").'</option>';
                                $property_types = get_categories("taxonomy=property_type&hide_empty=1");
                                foreach ( $property_types as $property_type ) {
                                    $id = esc_attr( $property_type->term_id );
                                    $title = esc_html( $property_type->name );
                                    $selected = "";
                                    echo  "<option value='{$id}' {$selected}>{$title}</option>";
                                }        
                        echo '</select>';
                        echo '</div>';
                    endif;

                    #Location Module
                    if( array_key_exists("enable-property-location-module-for-{$key}", $property) ):
                        echo '<div class="location-module medium-module">';
                        echo '<label>'.__('Locations','dt_themes').'</label>';
                        echo '<select name="plocation">';
                        echo '<option value="0">'.__("Location","dt_themes").'</option>';
                                $property_locations = get_categories("taxonomy=property_location&hide_empty=1");
                                foreach ( $property_locations as $property_location ) {
                                    $id = esc_attr( $property_location->term_id );
                                    $title = esc_html( $property_location->name );
                                    $selected = "";
                                    echo  "<option value='{$id}' {$selected}>{$title}</option>";
                                }        
                        echo '</select>';
                        echo '</div>';
                    endif;

                    #Min price Module
                    if( array_key_exists("enable-property-min-price-for-{$key}", $property) ):
                        echo '<div class="min-price-module medium-module">';
                        echo '<label>'.__('Min Price','dt_themes').'</label>';
                        echo '<select name="minprice">';
                        echo '<option value="0">'.__("Any","dt_themes").'</option>';
                                $min_prices = array_key_exists("min-price-for-{$key}", $property) ? $property["min-price-for-{$key}"] : array();
								$min_prices = array_filter($min_prices);
								$min_prices = array_unique($min_prices);
                                foreach ( $min_prices as $min_price ) {
                                    $selected = "";
                                    echo  "<option value='".esc_attr($min_price)."' {$selected}>".esc_html($min_price)."</option>";
                                }        
                        echo '</select>';
                        echo '</div>';
                    endif;

                    #Max price Module
                    if( array_key_exists("enable-property-max-price-for-{$key}", $property) ):
                        echo '<div class="max-price-module medium-module">';
                        echo '<label>'.__('Max Price','dt_themes').'</label>';
                        echo '<select name="maxprice">';
                        echo '<option value="0">'.__("Any","dt_themes").'</option>';
                                $max_prices = array_key_exists("max-price-for-{$key}", $property) ? $property["max-price-for-{$key}"] : array();
								$max_prices = array_filter($max_prices);
								$max_prices = array_unique($max_prices);
                                foreach ( $max_prices as $max_price ) {
                                    $selected = "";
                                    echo  "<option value='".esc_attr($max_price)."' {$selected}>".esc_html($max_price)."</option>";
                                }        
                        echo '</select>';
                        echo '</div>';
                    endif;

                    #Bed
                    if( array_key_exists("enable-property-beds-meta-for-{$key}", $property) ):
                        echo '<div class="beds-module small-module">';
                        echo '<label>'.__('Beds','dt_themes').'</label>';
                        echo '<select name="pbeds">';
                        echo '<option value=">0">'.__("Any","dt_themes").'</option>';
                                $beds = array_key_exists("beds-for-{$key}", $property) ? $property["beds-for-{$key}"]: array();
                                $beds = array_filter($beds);
                                $beds = array_unique( $beds);
                                foreach ( $beds as $bed ) {
                                    $selected = "";
                                    echo  "<option value='".esc_attr($bed)."' {$selected}>".esc_html($bed)."</option>";
                                }        
                        echo '</select>';
                        echo '</div>';
                    endif;

                    #Bath
                    if( array_key_exists("enable-property-baths-meta-for-{$key}", $property) ):
                        echo '<div class="baths-module small-module">';
                        echo '<label>'.__('Baths','dt_themes').'</label>';
                        echo '<select name="pbaths">';
                        echo '<option value=">0">'.__("Any","dt_themes").'</option>';
                                $baths = array_key_exists("baths-for-{$key}", $property) ? $property["baths-for-{$key}"]: array();
                                $baths = array_filter($baths);
                                $baths = array_unique( $baths);
                                foreach ( $baths as $bath ) {
                                    $selected = "";
                                    echo  "<option value='".esc_attr($bath)."' {$selected}>".esc_html($bath)."</option>";
                                }        
                        echo '</select>';
                        echo '</div>';
                    endif;
                    
                    #Floors
                    if( array_key_exists("enable-property-floors-meta-for-{$key}", $property) ):
                        echo '<div class="floors-module small-module">';
                        echo '<label>'.__('Floors','dt_themes').'</label>';
                        echo '<select name="pfloors">';
                        echo '<option value=">0">'.__("Any","dt_themes").'</option>';
                                $floors = array_key_exists("floors-for-{$key}", $property) ? $property["floors-for-{$key}"]: array();
                                $floors = array_filter($floors);
                                $floors = array_unique( $floors);
                                foreach ( $floors as $floor ) {
                                    $selected = "";
                                    echo  "<option value='".esc_attr($floor)."' {$selected}>".esc_html($floor)."</option>";
                                }        
                        echo '</select>';
                        echo '</div>';
                    endif;
                    
                    #Garages
                    if( array_key_exists("enable-property-garages-meta-for-{$key}", $property) ):
                        echo '<div class="garages-module small-module">';
                        echo '<label>'.__('Garages','dt_themes').'</label>';
                        echo '<select name="pgarages">';
                        echo '<option value=">0">'.__("Any","dt_themes").'</option>';
                                $garages = array_key_exists("garages-for-{$key}", $property) ? $property["garages-for-{$key}"]: array();
                                $garages = array_filter($garages);
                                $garages = array_unique( $garages);
                                foreach ( $garages as $garage ) {
                                    $selected = "";
                                    echo  "<option value='".esc_attr($garage)."' {$selected}>".esc_html($garage)."</option>";
                                }        
                        echo '</select>';
                        echo '</div>';
                    endif;                    


                    echo '<input type="submit" name="dt-propery-search-submit" value="'.__("Search","dt_themes").'" />';
                    echo '</form>';
                    echo '</div>';
                endif;
            }?>
    </div>
</div><!-- **Property Search Container - End** -->