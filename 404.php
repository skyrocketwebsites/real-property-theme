<?php get_header();?>
<?php $page_layout =  dttheme_option('specialty','404-layout');
	  $show_sidebar = false;
	  $sidebar_class="";

	switch($page_layout):
		case 'full-width':
			$page_layout 	= 	"content-full-width";
		break;
		
		case 'with-left-sidebar':
			$page_layout = "page-with-sidebar with-left-sidebar";
			$show_sidebar = $show_left_sidebar = true;
			$sidebar_class = "secondary-has-left-sidebar";
		break;
		
		case 'with-right-sidebar':
			$page_layout = "page-with-sidebar with-right-sidebar";
			$show_sidebar = $show_right_sidebar	= true;
			$sidebar_class = "secondary-has-right-sidebar";
		break;
	endswitch;

	if ( $show_sidebar ):
		if ( $show_left_sidebar ): ?>
			<!-- Secondary Left -->
			<section id="secondary-left" class="secondary-sidebar <?php echo esc_attr($sidebar_class);?>"><?php get_sidebar( 'left' );?></section><?php
		endif;
	endif;?>

	<!-- ** Primary Section ** -->
	<section id="primary" class="<?php echo esc_attr($page_layout);?>">
		<div class="error-info"><?php
			echo dt_wp_kses( stripcslashes(dttheme_option('specialty','404-message') ) );?>
			<a href="<?php echo home_url();?>" title="" class="dt-sc-button small"><?php _e('Back to Home','dt_themes');?></a> 
		</div>
	</section><!-- ** Primary Section End ** --><?php

	if ( $show_sidebar ):
		if ( $show_right_sidebar ): ?>
			<!-- Secondary Right -->
			<section id="secondary-right" class="secondary-sidebar <?php echo esc_attr($sidebar_class);?>"><?php get_sidebar( 'right' );?></section><?php
		endif;
	endif;
get_footer(); ?>