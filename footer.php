    <?php if( !is_page_template( 'tpl-fullwidth.php' ) ): ?>
            </div><!-- **Container - End** -->
    <?php endif;?>
        </div><!-- **Main - End** -->


        <?php $dttheme_options = get_option(IAMD_THEME_SETTINGS); $dttheme_general = $dttheme_options['general'];?>
        <!-- **Footer** -->
        <footer id="footer"><?php
        	if(!empty($dttheme_general['show-footer'])): ?>
        	<div class="footer-widgets-wrapper">
        		<div class="container"><?php dttheme_show_footer_widgetarea($dttheme_general['footer-columns']);?></div>
        	</div><?php
        	endif;?>

        	<div class="copyright">
        		<div class="container"><?php
        			if( !empty($dttheme_general['show-copyrighttext']) ):
        				echo '<div class="copyright-info">';
        				echo  dt_wp_kses(stripslashes($dttheme_general['copyright-text']));
        				echo '</div>'; 
        			endif;?>

        			<div class="footer-links">
                    <p><?php _e('Follow us','dt_themes');?></p>
                    <?php echo do_shortcode('[social/]');?></div>
        		</div>
        	</div>
        </footer><!-- **Footer - End** -->
    </div><!-- **Inner Wrapper - End** -->
</div><!-- **Wrapper - End** -->
<?php
	if (is_singular() AND comments_open())
		wp_enqueue_script( 'comment-reply');

	if(dttheme_option('integration', 'enable-body-code') != '') :
        echo '<script type="text/javascript">';
		echo  dt_wp_kses( stripslashes(dttheme_option('integration', 'body-code')) );
        echo '</script>';
    endif;
	wp_footer(); ?>
</body>
</html>