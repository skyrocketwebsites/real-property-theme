<?php get_header();
	$page_layout  = dttheme_option( 'property', 'agency-archive-page-layout' );
	$page_layout  = !empty( $page_layout ) ? $page_layout : "content-full-width";
	$show_sidebar = $show_left_sidebar = $show_right_sidebar =  false;
	$sidebar_class = "";

	switch ( $page_layout ) {

		case 'with-left-sidebar':
			$page_layout = "page-with-sidebar with-left-sidebar";
			$show_sidebar = $show_left_sidebar = true;
			$sidebar_class = "secondary-has-left-sidebar";
		break;

		case 'with-right-sidebar':
			$page_layout = "page-with-sidebar with-right-sidebar";
			$show_sidebar = $show_right_sidebar = true;
			$sidebar_class = "secondary-has-right-sidebar";
		break;

		case 'both-sidebar':
			$page_layout = "page-with-sidebar page-with-both-sidebar";
			$show_sidebar = $show_right_sidebar = $show_left_sidebar = true;
			$sidebar_class = "secondary-has-both-sidebar";
		break;

		case 'content-full-width':
		default:
			$page_layout = "content-full-width";
		break;
	}

	if ( $show_sidebar ):
		if ( $show_left_sidebar ): ?>
			<!-- Secondary Left -->
			<section id="secondary-left" class="<?php echo esc_attr($sidebar_class);?>"><?php get_sidebar( 'left' );?></section><?php
		endif;
	endif;?>
		<section id="primary" class="<?php echo esc_attr($page_layout);?>"><?php

			$post_layout = dttheme_option('property','agency-archive-post-layout'); 
			$post_layout = !empty($post_layout) ? $post_layout : "one-half-column";
			$post_class = "";

			switch($post_layout):

				case 'one-column':
					$post_class = $show_sidebar ? " column dt-sc-one-column with-sidebar" : " column dt-sc-one-column ";
				break;

				case 'one-half-column';
					$post_class = $show_sidebar ? "  column dt-sc-one-half with-sidebar " : "  column dt-sc-one-half ";
					$columns = 2;
				break;

				case 'one-third-column':
					$post_class = $show_sidebar ? "  column dt-sc-one-third with-sidebar " : "  column dt-sc-one-third ";
					$columns = 3;
				break;

				case 'one-fourth-column':
					$post_class = $show_sidebar ? "  column dt-sc-one-fourth with-sidebar " : " column dt-sc-one-fourth";
					$columns = 4;
				break;
			endswitch;


			if( have_posts() ):
				$i = 1;
				while( have_posts() ):
					the_post();
					$id = get_the_ID();

					if($i == 1) $temp_class = $post_class." first"; else $temp_class = $post_class;
					if($i == $columns) $i = 1; else $i = $i + 1;?>
					<div class="<?php echo esc_attr($temp_class);?>">

						<div class="dt-sc-agency-list dt-sc-agents-list">

							<div class="dt-sc-agent-thumb"><?php
								if( has_post_thumbnail() ) :
									the_post_thumbnail($id,"full");
								else:?>
									<img src="http://placehold.it/400x420&text=Image" alt="<?php printf(esc_attr__('%s'),the_title_attribute('echo=0'));?>" title="<?php printf(esc_attr__('%s'),the_title_attribute('echo=0'));?>"/><?php
								endif;?>	
							</div>

							<div class="dt-sc-agent-details">

								<h4><a href="<?php the_permalink();?>" title="<?php printf(esc_attr__('%s'),the_title_attribute('echo=0'));?>"><?php the_title();?></a></h4>

								<div class="dt-sc-agent-contact"><?php
									$agency_address = get_post_meta ( $id, "_agency_address",true);
									$agency_address = dt_wp_kses( $agency_address ); 

									$agency_mobile = get_post_meta ( $id, "_agency_mobile",true);
									$agency_mobile = dt_wp_kses( $agency_mobile ); 

									$agency_phone = get_post_meta ( $id, "_agency_phone",true);
									$agency_phone = dt_wp_kses( $agency_phone ); 

									$agency_email_id = get_post_meta ( $id, "_agency_email_id",true);
									$agency_email_id = dt_wp_kses( $agency_email_id ); 

									if( !empty($agency_address) )
										echo "<p> <span class='fa fa-map-marker'> </span> {$agency_address} </p>";

									if( !empty( $agency_mobile ) )
										echo "<p> <span class='fa fa-mobile'> </span> {$agency_mobile} </p>";

									if( !empty( $agency_phone ) )
										echo "<p> <span class='fa fa-phone'> </span> {$agency_phone} </p>";

									if( !empty( $agency_email_id ) )
										echo "<p> <span class='fa fa-envelope'> </span> <a href='mailto:{$agency_email_id}'>{$agency_email_id}</a></p>";?>
								</div>

								<div class="dt-sc-agent-content"><?php the_content();?></div>

								<?php $socials =  get_post_meta( $id, "_agent_social",true);
									if( !empty($socials) ) :
										echo '<ul class="dt-sc-social-icons">';
										foreach( $socials as $k => $v ):
											$i1 = IAMD_BASE_URL."images/sociable/hover/{$k}";
											$i2 = IAMD_BASE_URL."images/sociable/{$k}";
											$class = explode(".",$k);
											$class = $class[0];
											echo "<li class='".esc_attr($class)."'><a href='".esc_url($v)."'><img src='".esc_url($i1)."'/><img src='".esc_url($i2)."'/></a></li>";
										endforeach;
										echo '</ul>';
									endif;?>
							</div>
						</div>
					</div><?php
				endwhile;	
			else:?>
				<div class="dt-sc-hr-invisible"> </div>
				<h1><?php _e( 'Nothing Found','dt_themes'); ?></h1>
				<h3><?php _e( 'Apologies, but no results were found for the requested archive.', 'dt_themes'); ?></h3>
				<?php get_search_form();
			endif;?>
	        <div class="dt-sc-clear"></div>
			<!-- **Pagination** -->
			<div class="pagination">
				<div class="prev-post"><?php previous_posts_link('<span class="fa fa-angle-double-left"></span> Prev');?></div>
				<?php echo dttheme_pagination();?>
				<div class="next-post"><?php next_posts_link('Next <span class="fa fa-angle-double-right"></span>');?></div>
			</div><!-- **Pagination - End** -->
		</section><?php
	if ( $show_sidebar ):
		if ( $show_right_sidebar ): ?>
			<!-- Secondary Right -->
			<section id="secondary-right" class="<?php echo esc_attr($sidebar_class);?>"><?php get_sidebar( 'right' );?></section><?php
		endif;
	endif;
get_footer(); ?>